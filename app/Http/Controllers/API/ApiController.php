<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use Validator;
use Mail;
use App\User;
use App\Task;
use App\SubTask;
use Hash;
use Auth;
use Illuminate\Support\Facades\Crypt;
use File;

class ApiController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

   public function userLogin(Request $request)
    { 
      $validator = Validator::make($request->all(), [
           'email' => 'required',
           'password' => 'required',
      ]);
      if ($validator->fails()) {
       $error = $validator->errors()->all();
       return response()->json(['status'=>'failed','error'=>$error],201);
      }
      $email = $request->input('email');
      $credentials = request(['email', 'password']);
      //dd($email,$credentials);
      if(!Auth::attempt($credentials))
          return response()->json(['status'=>'failed','message'=>'Wrong Credentials!'],201);
      $user = User::where('email', $email)->select('id','name','email')->first();
      //dd('swds');
      $tokenResult = $user->createToken('Personal Access Token');
      //dd($tokenResult);
      $token = $tokenResult->token;
      // if ($request->remember_me)
      //     $token->expires_at = Carbon::now()->addWeeks(1);
      $token->save();
      return response()->json(['status' => 'success','message'=>'Login successfully.','data'=>$user, 'access_token' => $tokenResult->accessToken, 'token_type' => 'Bearer', 'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()],200);
    } 

    public function update_pic(Request $request)
    {
      $validator = Validator::make($request->all(), [
                                           'email' => 'required',
                                           'profile_pic' => 'required',

                                     ]);
      if ($validator->fails()) {
             $error = $validator->errors()->all();
             return response()->json(['status'=>'failed','error'=>$error],201);
      }

      $user = User::where('email',$request->input('email'))->first();
      $path = public_path('user_profile/');
      $url= env('APP_URL').'public/user_profile/';
      //dd($url);
      if($request->file('profile_pic')){
            $app_icon = $request->file('profile_pic');
            $extension = strtolower($request->file('profile_pic')->getClientOriginalExtension());
            $filename = $user->id.'profile_pic'.time().'.'. $extension;
            if(!File::isDirectory($path)){
              File::makeDirectory($path);
            }           
            $app_icon->move($path,$filename);
            //Storage::disk('images')->put('filename', $filename);
            $user->profile_pic = $url.$filename;
            $user->save();
              }

      return response()->json(['status' => 'success','message'=>'User picture updated successfully.'],200);

    }

    public function profile_info(Request $request)
    {
      //dd('huj');
      $validator = Validator::make($request->all(), [
                                           'email' => 'required',

                                     ]);
      if ($validator->fails()) {
             $error = $validator->errors()->all();
             return response()->json(['status'=>'failed','error'=>$error],201);
      }

      $user = User::where('email',$request->input('email'))->first();
      
      if(!$user)
      {
        return response()->json(['status'=>'failed','message'=>'Please check email address!'],201);
      }

      $data = [  'fname'=> $user->first_name,
                 'lname'=> $user->last_name,
                 'email'=> $user->email,
                 'mobile'=> $user->phone,
                 'pic'=> $user->profile_pic ? $user->profile_pic : ''
                
              ];
      
      return response()->json(['status' => 'success','message'=>'User profile data.','user'=>$data],200);

    }
    
    public function update_profile(Request $request)
    {
      //dd('huj');
      $validator = Validator::make($request->all(), [
           'email' => 'required',
      ]);
      if ($validator->fails()) {
             $error = $validator->errors()->all();
             return response()->json(['status'=>'failed','error'=>$error],201);
      }
     $user = User::where('email',$request->input('email'))->update(['first_name'=>$request->input('first_name'),
              'last_name'=>$request->input('last_name'),'name'=>$request->input('first_name').' '.$request->input('last_name'),'email'=>$request->input('email'),'phone'=>$request->input('mobile')]);
      
      if(!$user)
      {
        return response()->json(['status'=>'failed','message'=>'Please check email address!'],201);
      }
      return response()->json(['status' => 'success','message'=>'User profile updated successfully.'],200);
    }

    public function generateOTP(Request $request) {
      $email = $request->email;
      $checkUser = User::where('email',$email)->first();
      if($checkUser) {
        $otp = mt_rand(1000,9999);
        // $checkUser->id = 
        $checkUser->otp = $otp;
        $checkUser->save();
        try
        {       
          Mail::send('email.otp', ['email' => $email,'otp' => $otp], function ($m) use ($email) {
            $m->from(env("MAIL_FROM_ADDRESS", "rajat.kumar@udaantechnologies.info"), 'Cleaning Wizard');

            $m->to($email)->subject('Cleaner OTP.');
          });
          return response()->json(['status'=>'success','message'=>'An OTP has been sent to email id provided'],200);
        }
        catch(\Exception $e){ 
          return response()->json(['status' => 'Failed','message'=>'Mail not sent'],201);
          // return response()->json(['status'=>'failed','message'=>'An OTP has been sent to email id provided'],200);
        } 
      }
      else {
        return response()->json(['status'=>'failed','message'=>'Email not exist!'],201);
      }
    }

    public function verifyOTP(Request $request) { 
      $email = $request->email;
      $userOtp = $request->otp;
      $checkUser = User::where('email',$email)->first();
      if($checkUser) { 
        if($userOtp == $checkUser["otp"]) {
          return response()->json(['status'=>'success','message'=>'OTP is verified'],200);
        }
        else {
          return response()->json(['status'=>'failed','message'=>'Invalid OTP'],201);  
        }
      }
      else {
        return response()->json(['status'=>'failed','message'=>'Email not exist!'],201);
      }
    }
    public function resetPassword(Request $request) {
      $email = $request->email;
      $password = $request->new_password;
      $checkUser = User::where('email',$email)->first();
      if($checkUser) {  
        $checkUser->password = Hash::make($password);
        $checkUser->save();
        try
        {       
          Mail::send('email.login', ['email' => $email,'password'=> $password], function ($m) use ($email) {
            $m->from(env("MAIL_FROM_ADDRESS", "rajat.kumar@udaantechnologies.info"), 'Cleaning Wizard');

            $m->to($email)->subject('Cleaner New Password.');
          });
        }
        catch(\Exception $e){
          // return response()->json(['status' => 'Failed','message'=>'Mail not sent'],200);
        } 
        return response()->json(['status'=>'success','message'=>'Password has been reset successfully.'],200);
      }
      else {
        return response()->json(['status'=>'failed','message'=>'Email not exist!'],201);
      }
      
    }

    public function update_password(Request $request)
    {
      //dd('ds');
      $validator = Validator::make($request->all(), [
           'email' => 'required',
           'old_password' => 'required',
           'new_password' => 'required',
      ]);
      if ($validator->fails()) {
             $error = $validator->errors()->all();
             return response()->json(['status'=>'failed','error'=>$error],201);
      }

      $email = $request->email; 
      $old_password = $request->old_password; 
      $new_password = $request->new_password; 
      $user = User::where('email',$email)->first();
      if($user)
      {
        if(\Hash::check($old_password , $user->password ))
        {
          User::where('email',$email)->update(['password' => Hash::make($new_password)]);

          return response()->json(['status'=>'success','message'=>'Password has been updated successfully.'],200);
        }else{
          return response()->json(['status'=>'failed','message'=>'The password provided is wrong.'],201);
        } 
      }else{
          return response()->json(['status'=>'failed','message'=>'Email not exist!'],201);
      }  
    }

    public function get_citylist(Request $request)
    {
      $validator = Validator::make($request->all(), [
           'email' => 'required',
      ]);
      if ($validator->fails()) {
             $error = $validator->errors()->all();
             return response()->json(['status'=>'failed','error'=>$error],201);
      }

      $email = $request->email;
      $city = [];
      $cleaner = User::with('userLocation.location')->where('email',$email)->first();
      if($cleaner)
      {
        foreach ($cleaner->userLocation as $user_location)
        {
          if(!in_array($user_location->location->city,$city))
          {
            array_push($city,$user_location->location->city);
          }
          
        }

        return response()->json(['status'=>'success','message'=>'cities list.','cities'=>$city],200);
      }else{
        return response()->json(['status'=>'failed','message'=>'Email not exist!'],201);
      }
      
    }

    public function get_addresslist(Request $request)
    {
      $validator = Validator::make($request->all(), [
           'email' => 'required',
           'city' => 'required',
      ]);
      if ($validator->fails()) {
             $error = $validator->errors()->all();
             return response()->json(['status'=>'failed','error'=>$error],201);
      }

      $email = $request->email;
      $city = $request->city;
      $addresses = [];
      $cleaner = User::with('userLocation.location')->where('email',$email)->first();
      
      //dd($cleaner);
      if($cleaner)
      {
        foreach ($cleaner->userLocation as $user_location)
        {

          if($user_location->location->city == $city)
          { 
            if(!in_array($user_location->location->area,$addresses))
            {
              /*$task = Task::where('address',$user_location->location->id)->where('user_id',$cleaner->id)->first();*/
              //array_push($addresses,$user_location->location->area);
              $addresses[] = array(
                              'address_id' => $user_location->location->id,
                              'address' => $user_location->address ? $user_location->address : '',
                              'city' => $user_location->location->city,
                              'zip' => $user_location->location->zip,
                              'contact_person' => $user_location->contact_person ? $user_location->contact_person : '',
                              'contact_number' => $user_location->contact_number ? $user_location->contact_number : '',
                              'door_unlock_code' => $user_location->door_unlock_code ? $user_location->door_unlock_code : '',
                              'curfew_time' => $user_location->location->curfew_time,
                           );
            }
          }
          
        }

        return response()->json(['status'=>'success','message'=>'addresses list.','addresses'=>$addresses],200);
      }else{
        return response()->json(['status'=>'failed','message'=>'Email not exist!'],201);
      }
      
    }

    public function get_roomlist(Request $request)
    {
      $validator = Validator::make($request->all(), [
           'email' => 'required',
           'address_id' => 'required',
      ]);
      if ($validator->fails()) {
             $error = $validator->errors()->all();
             return response()->json(['status'=>'failed','error'=>$error],201);
      }
      $address_id = $request->address_id;
      $email = $request->email;
      $user = User::where('email',$email)->first();
      if(!$user)
      {
        return response()->json(['status'=>'failed','message'=>'Email not exist!'],201);
      }
      $user_id = $user->id;

      $lists = Task::with('room')->where('user_id',$user_id)->where('city_area_id',$address_id)->get();
      $room = [];
      foreach ($lists as $list)
      {
        $room[] = array(
                          'room_id' => $list->room_id,
                          'room_name' => $list->room->room_type,
                       );
      }

      return response()->json(['status'=>'success','message'=>'room list.','rooms'=>$room],200);
    }

    public function get_tasklist(Request $request)
    {
      $validator = Validator::make($request->all(), [
           'email' => 'required',
           'address_id' => 'required',
           'room_id' => 'required',
      ]);
      if ($validator->fails()) {
             $error = $validator->errors()->all();
             return response()->json(['status'=>'failed','error'=>$error],201);
      }
      $address_id = $request->address_id;
      $room_id = $request->room_id;
      $email = $request->email;
      $user = User::where('email',$email)->first();
      if(!$user)
      {
        return response()->json(['status'=>'failed','message'=>'Email not exist!'],201);
      }
      $user_id = $user->id;

      $task = Task::where('user_id',$user_id)->where('city_area_id',$address_id)->where('room_id',$room_id)->first();
      $sub_task = SubTask::where('task_id',$task->id)->select('id as sub_task_id','task_name as sub_task_name','status')->get();

      return response()->json(['status'=>'success','message'=>'task list.','task_id'=>$task->id,'task_name'=>$task->task_name,'subTasks'=>$sub_task],200);
    }

    public function update_task(Request $request)
    {
      $task_id = $request->task_id;
      $subtasks = $request->subtasks;
      $comment = $request->comment;

      if(!$task_id)
      {
        return response()->json(['status'=>'failed','message'=>'The task id field is required.'],201);
      }
      if(!$subtasks)
      {
        return response()->json(['status'=>'failed','message'=>'The subtasks field is required.'],201);
      }

      $task = Task::where('id',$task_id)->update(['comments'=>$comment]);
      if(!$task)
      {
        return response()->json(['status'=>'failed','message'=>'Task not exist!'],201);
      }
      foreach ($subtasks as $subtask)
      {
        $sub_task = SubTask::where('id',$subtask['subtask_id'])->update(['status'=>$subtask['status']]);
      }


      return response()->json(['status'=>'success','message'=>'Room Status updated successfully.'],200);
    }

}
