<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Location;
use App\Product;
use Illuminate\Support\Facades\DB;
use Validator;


class LocationApiController extends BaseController
{

   /* public function index(Payment $payment)
    {
        return view('payments.index', ['payments' => $payment->paginate(15)]);
    } */

    public function getLocations(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'product_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $lat = $request->lat;
        $lng = $request->lng;
        $product_id = $request->product_id;

        $product_list = DB::select( DB::raw("SELECT a.*,b.product_id, ( 3959 * acos( cos( radians(".$lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance FROM locations as a left join products as b on b.location_id=a.id where  b.product_id='".$product_id."' having distance < 50 ORDER BY distance LIMIT 0 , 20") );
       // $product_list = Product::join('locations','locations.id','=','products.location_id')->where('product_id', $input['product_id'])->get();

        if(count($product_list)){
             return $this->sendResponse($product_list, 'Mapped List.');
        }else{
            return $this->sendResponse($product_list, 'No Location have mapped for that product.');
        }
       
    }

    public function updatePayment($id, Request $request, Payment $payment)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'bank_name' => 'required',
            'payment_amount' => 'required',
            'payment_mode' => 'required',
            'payment_from' => 'required',
            'payment_status' => 'required',
            'transaction_id' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
       
        $payment=Payment::where('id', $id)->update($input);
        if($payment){
            return $this->sendResponse($request->toArray(), 'payment updated successfully.'); 
        }else{
            return $this->sendError('Something went wrong.'); 
        }
    }

    public function deletePayment($id)
    {
        $payment = Payment::find($id);
        if($payment){
            $payment->delete();
            return $this->sendResponse($payment->toArray(), 'Payment deleted successfully.');
        }else{
            return $this->sendError('Record not exist'); 
        } 
    }
}
