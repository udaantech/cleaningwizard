<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Location;
use Mail;
use Hash;
use Session;
use Carbon\Carbon;

class LocationController extends Controller
{
  public function index(Request $request)
  {
    $zip_query = $request->query('zip');
    $city_query = $request->query('city');
    //dd($zip_query,$city_query);
    if($zip_query && $city_query == null)
    {
      $locations = Location::where('zip',$zip_query)->paginate(10);
    }
    else if($zip_query == null && $city_query){
      $locations = Location::where('city',$city_query)->paginate(10);
    }
    else if($zip_query && $city_query){
      $locations = Location::where('city',$city_query)->where('zip',$zip_query)->paginate(10);
    }
    else{
      $locations = Location::paginate(10);
    }
    $hours = [1,2,3,4,5,6,7,8,9,10,11,12];
    $cities = Location::get()->unique('city');
    $all_zip = Location::get()->unique('zip');
    return view('location.index',compact('locations','hours','cities','all_zip','zip_query','city_query'));
  }

  public function store(Request $request)
  {
    //dd($request->all());
    


  	$location = new Location();
    $location->city = ucfirst($request->city);
  	$location->area = ucfirst($request->area);
  	$location->zip = $request->zip;
  	$location->curfew_time = $request->start_time.' '.$request->start_ampm.' to '.$request->end_time.' '.$request->end_ampm;
  	$location->save();
 
    Session::flash('success','Location/Area created Successfully.');
    return redirect()->route('location.index');
  }

  public function update(Request $request)
  {
  	$location = Location::whereId($request->location_id)->first();
  	$location->city = ucfirst($request->city);
    $location->area = ucfirst($request->area);
    $location->zip = $request->zip;
    $location->curfew_time = $request->start_time.' '.$request->start_ampm.' to '.$request->end_time.' '.$request->end_ampm;
    $location->save();
    Session::flash('success','Location/Area updated Successfully.');
    return redirect()->route('location.index');
  }

  public function delete(Request $request)
  {
  	$location = Location::whereId($request->location_id)->delete();
  	Session::flash('success','Location/Area deleted Successfully.');
    return redirect()->route('location.index');
  }

}
