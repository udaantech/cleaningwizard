<?php

namespace App\Http\Controllers;

use App\Product;
use App\Location;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Oseintow\Shopify\Facades\Shopify;
class ProductController extends Controller
{
  protected $api_key;
  protected $password;
  protected $shop_url;
  protected $secret_key;
 
  public function __construct() {
    $this->api_key = env('SHOPIFY_APIKEY');
    $this->password = env('SHOPIFY_PASSWORD');
    $this->secret_key = env('SHOPIFY_SECRET');
    $this->shop_url = env('SHOPIFY_SHOP_URL');
  }

  public function index(Product $model,Request $request)
  {
    $shopify=$this->getShopifyObject();
    $shopify_products1 = $shopify->get("admin/products.json", ['fields'=>'id,title,variants',"page" => 1, "published_status"=>"published"]);
    $shopify_products = json_decode($shopify_products1,true);
    foreach($shopify_products as $key=>$eachProduct){
      $product_list = Product::join('locations','locations.id','=','products.location_id')->where('product_id', strval($eachProduct['id']))->get();

      foreach($product_list as $eachRecord){
        $shopify_products[$key]['location_details'][] = array(
          'location_id' => $eachRecord->location_id, 
          'center_name' => $eachRecord->center_name, 
          'address' => $eachRecord->address 
        );
      }  
    }
    $shopify_products=json_decode(json_encode($shopify_products), FALSE);

    $location_list = Location::all();
    return view('product.index', compact('shopify_products','location_list'));
  }

  public function create(Request $request)
  {
    $product_id=$request->query('product_id');
    $product_name=$request->query('product_name');
    $map_location_id = DB::table('products')->select('location_id')->where('product_id',$product_id)->get(); 
    //$location_list = Location::all();
    $active_location_list = Location::all()->where('location_status','Active');
    $active_location_list = Location::all()->where('location_status','Active');
    $inactive_location_list = Location::all()->where('location_status','Inactive');
    return view('product.create', compact('active_location_list','inactive_location_list','product_name','map_location_id','product_id'));
  }

  public function store(Request $request,Product $model)
  {
    $data=$request->all();
    $product_maper = array();
    //Product::where('product_id',$data['product_id'])->delete();
    if(!empty($data['location_ids'])){
      foreach($data['location_ids'] as $location_id){
        $location_id_exist = DB::table('products')->select('location_id')->where('product_id',$data['product_id'])->where('location_id',$location_id)->get();
        if(count($location_id_exist)==0){
          $product_maper[] = array(
              'location_id' => $location_id,
              'product_id' =>  $data['product_id']
          );
        }
       // $location = Product::create($product_maper);
      }
      if(count($product_maper) <>0){
        Product::insert($product_maper);
        return redirect()->route('product.index')->withStatus(__('Location successfully mapped with product.'));
      }
      return redirect()->route('product.index');
    }
    return redirect()->route('product.index')->withStatus(__('No location not mapped with that product.'));
  }

  public function destroy($location_id,Request $request)
  {
      $product_id = $request->query('product_id');
      Product::where('product_id',$product_id)->where('location_id',$location_id)->delete();
      return redirect()->route('product.index')->withStatus(__('Mapped Location Deleted Successfully.'));
  }
  public function getShopifyObject()
  {
    return Shopify::setShopUrl($this->shop_url)->setAccessToken($this->password);
  }    
}
