<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Http\Request;
use Session;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        return view('profile.edit');
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request) 
    {
        auth()->user()->update($request->all());
        Session::flash('success','Profile successfully updated.');
        return back();
    }


    public function imageUpdate(Request $request) 
    {
        if($request->file('profile_image')){
            $profile_image = $request->file('profile_image');
            //dd($profile_image);
            $extension = strtolower($request->file('profile_image')->getClientOriginalExtension());
            $filename = $request->name.'image_'.time() . '.' . $extension;
            $path = public_path('black/img/');
            $profile_image->move($path,$filename);
        }
        auth()->user()->where('id', auth()->user()->id)->update(['profile_image' => $filename]);
        return back()->withImageStatus(__('Profile Image successfully updated.'));
    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        auth()->user()->update(['password' => Hash::make($request->get('password'))]);
        Session::flash('success','Password successfully updated.');
        return back();
    }
}
