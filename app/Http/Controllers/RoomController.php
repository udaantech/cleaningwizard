<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Room;
use Mail;
use Hash;
use Session;
use Carbon\Carbon;

class RoomController extends Controller
{
  public function index(Request $request)
  {
    $rooms = Room::paginate(10);
    $all_room = Room::all();    //for client side validation
    return view('room.index',compact('rooms','all_room'));
  }

  public function store(Request $request)
  {
  	$room = new Room();
    $room->room_type = ucfirst($request->room_type);
  	$room->save();
 
    Session::flash('success','Room type created Successfully.');
    return redirect()->route('room.index');
  }

  public function update(Request $request)
  {
  	$room = Room::whereId($request->room_id)->first();
  	$room->room_type = ucfirst($request->room_type);
    $room->save();
    Session::flash('success','Room type updated Successfully.');
    return redirect()->route('room.index');
  }

  public function delete(Request $request)
  {
  	$room = Room::whereId($request->room_id)->delete();
  	Session::flash('success','Room deleted Successfully.');
    return redirect()->route('room.index');
  }

}
