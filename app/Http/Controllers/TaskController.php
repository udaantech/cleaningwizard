<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Location;
use App\UserLocation;
use App\Room;
use App\Task;
use App\SubTask;
use App\User;
use Session;
use Carbon\Carbon;

class TaskController extends Controller
{
  public function index(Request $request)
  { 
    //dd($$request->all());
    $cities = Location::select('city')->distinct('city')->get();
    $pincodes = Location::select('zip')->distinct('zip')->get();
    // $tasks = Task::with('subTasks','user','room','location','taskLocation')->get();
    // dd($tasks);
    $tasks = Task::with('subTasks','user','room','location','taskLocation')->orderBy('id','desc')->paginate(10);
    return view('task.index',compact('tasks','cities','pincodes'));
  }
  public function tasksFilter(Request $request)
  {
    //dd($request->all());
     
    //dd($start_date,$end_date);
    $tasks = Task::with('subTasks','user','room','location')->orderBy('id','desc')->where(function($query) use($request){
        $name = $request->search_name;
        $city = $request->search_city;
        $zip = $request->search_zip;
        $date_range = $request->date_range;
        if($date_range != "Select Date")
        {
          $date = explode(' - ',$date_range);
          $start_date = Carbon::parse($date[0])->format('Y-m-d 00:00:00'); 
          $end_date = Carbon::parse($date[1])->format('Y-m-d 23:59:59');
          $query->whereBetween('created_at',[$start_date,$end_date]);
        }
        if($name)
        {
          $query->whereHas('User',function($query1) use($name){$query1->where('name', 'like', '%'.$name.'%');});
        }
        
          $query->whereHas('Location',function($query2) use($city,$zip){
          if($city)
          {
            $query2->where('city', 'like', '%'.$city.'%');
          }   
          if($zip)
          {
            $query2->where('zip', 'like', '%'.$zip.'%');
          }
          });
    })->get();
    
    return view('task.task_filter',compact('tasks'))->render();
  }
  public function addNewTask(Request $request)
  {
  	$cityAreas = Location::select('id',DB::raw("CONCAT(locations.city,'/',locations.area) AS city_area"),'id')->get();
    $roomType = Room::select('id','room_type')->get();
    
  	// dd($cityArea);
    return view('task.new_task',compact('cityAreas','roomType'));
  }

  public function getCityUser($locationId) {
    $userList = '';
  	$users = UserLocation::where("location_id",$locationId)->with('users')->get();
    foreach ($users as $key => $user) {
      $userList .= '<option value="'.$user['users']['id'].'">'.$user['users']['name'].'</option>';
    }
    return $userList;
  }
  public function getCityAddress($locationId) {
    $location = '<option value="">Select Address</option>';
    $locations = UserLocation::select('id','address')->where("location_id",$locationId)->distinct()->get();
    foreach ($locations as $key => $loc) {
      if($loc['address'] != null) {
        $location .= '<option value="'.$loc['id'].'">'.$loc['address'].'</option>';
      }
    }
    return $location;
  }
  public function getroomType($address) {
    $roomType = '<option value="">Select Room</option>';
    $rooms = UserLocation::where("address",$address)->with('roomType')->get();
    foreach ($rooms as $key => $room) {
        $roomType .= '<option value="'.$room['roomType']['id'].'">'.$room['roomType']['room_type'].'</option>';
    }
    return $roomType;
  }
  public function getUserId($addressId) {
    $getUser = UserLocation::select('user_id')->where("id",$addressId)->first();
    if($getUser) {
      return response()->json(['status'=>'success','userId'=>$getUser->user_id],200);
    }
    return response()->json(['status'=>'error'],400);
  }
  public function store(Request $request)
  {
     //dd($request->all());
    $task = new Task();
    $task->user_id = $request->user_id;
    $task->city_area_id = $request->city_area_id;
    $task->room_id = $request->room_id;
    $task->task_name = ucfirst($request->task_name);
    $task->comments = null;
    $task->address = $request->address_id;
    // $task->door_unlock_code = $request->door_unlock_code;
    // $task->contact_person = $request->contact_person;
    // $task->contact_number = $request->contact_number;
    $task->save();
    $taskId = $task->id;
    $subTasks = $request->subTasks;
    $subtask = new SubTask();
    $data = [];
    foreach ($subTasks as $key => $subTask) {
      $data[] = [
        'task_id' => $taskId,
        'task_name' => ucfirst($subTask),
      ];
    }
    $subtask->insert($data);
    Session::flash('success','Task created Successfully.');
    return redirect()->route('task.index');
  }

  public function changeStatus(Request $request) {
    $statusChange = SubTask::where('id',$request->id)->update(['status'=>$request->status]);
    if($statusChange) {
      return response()->json(['status'=>'success'],201);
    }
    return response()->json(['status'=>'failed'],401);
  }

  public function deleteSubTask($id,$taskId) {
    $delete = SubTask::where('id',$id)->delete();
    if($delete) {
      $count = SubTask::where('task_id',$taskId)->count();
     return response()->json(['status'=>'success','count' => $count],201);
    }
    return response()->json(['status'=>'failed'],401);
  }
  public function updateSubTask(Request $request) {
    // dd($request->all());
    $updateSubTask = SubTask::where('id',$request->sub_task_id)->update(['task_name'=>ucfirst($request->sub_task),'status'=>$request->edit_status]);
    if($updateSubTask) {
      Session::flash('success','Sub Task updated Successfully.');
      return redirect()->route('task.index');
    }
  }
  public function exportTask(Request $request) {
    $tasks = Task::with('subTasks','user','room','location')->where(function($query) use($request){
      $name = $request->search_name;
      $city = $request->search_city;
      $zip = $request->search_zip;
      $date_range = $request->date_range;
      if($date_range)
      {
        $date = explode(' - ',$date_range);
        $start_date = Carbon::parse($date[0])->format('Y-m-d 00:00:00'); 
        $end_date = Carbon::parse($date[1])->format('Y-m-d 23:59:59');
        $query->whereBetween('created_at',[$start_date,$end_date]);
      }
      if($name)
      {
        $query->whereHas('User',function($query1) use($name){$query1->where('name', 'like', '%'.$name.'%');});
      }
      $query->whereHas('Location',function($query2) use($city,$zip){
        if($city)
        {
          $query2->where('city', 'like', '%'.$city.'%');
        }   
        if($zip)
        {
          $query2->where('zip', 'like', '%'.$zip.'%');
        }
      });
    })->get();
    $delimiter = ",";
    $filename = "report_" . date('Y-m-d') . ".csv";
    //create a file pointer
    $f = fopen('php://memory', 'w');
    //set column headers
    $fields = array('S.No','Task Name', 'Assigned To' ,'Address', 'City with Area', 'Pin Code' ,'Mobile Number', 'Room Type','Created At','Sub Task','Status');
    fputcsv($f, $fields, $delimiter);
    //output each row of the data, format line as csv and write to file pointer
    foreach ($tasks as $key => $task) {
      $area = $task['location']['area'].', '.$task['location']['city'];
      $lineData = array($key + 1, $task['task_name'],$task['user']['name'],$task['taskLocation']['address'],$area,$task['location']['zip'], $task['user']['phone'],ucfirst($task['room']['room_type']),date('d/m/Y', strtotime($task['created_at'])));
      fputcsv($f, $lineData, $delimiter);
      foreach ($task['subTasks'] as $key => $subTask) {
      	if($subTask['status'] == 0)
      	{
      		$status = 'Pending';
      	}
      	else if($subTask['status'] == 1)
      	{
      		$status = 'Issue';
      	}
      	else
      	{
      		$status = 'Completed';
      	}
      	$subTaskData = array('','','','','','','','','',$subTask['task_name'],$status);
      	fputcsv($f, $subTaskData, $delimiter);
      }
    }
    //move back to beginning of file
    fseek($f, 0);
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    // //output all remaining data on a file pointer
    fpassthru($f);
  }
  public function exportTasks(Request $request) {
    $tasks = Task::with('subTasks','user','room','location')->where(function($query) use($request){
      $name = $request->search_name;
      $city = $request->search_city;
      $zip = $request->search_zip;
      $date_range = $request->date_range;
      if($date_range)
      {
        $date = explode(' - ',$date_range);
        $start_date = Carbon::parse($date[0])->format('Y-m-d 00:00:00'); 
        $end_date = Carbon::parse($date[1])->format('Y-m-d 23:59:59');
        $query->whereBetween('created_at',[$start_date,$end_date]);
      }
      if($name)
      {
        $query->whereHas('User',function($query1) use($name){$query1->where('name', 'like', '%'.$name.'%');});
      }
      $query->whereHas('Location',function($query2) use($city,$zip){
        if($city)
        {
          $query2->where('city', 'like', '%'.$city.'%');
        }   
        if($zip)
        {
          $query2->where('zip', 'like', '%'.$zip.'%');
        }
      });
    })->get();
    $delimiter = ",";
    $filename = "report_" . date('Y-m-d') . ".csv";
    //create a file pointer
    $f = fopen('php://memory', 'w');
    //set column headers
    // $fields = array('S.No','Task Name', 'Assigned To' ,'Address', 'City with Area', 'Pin Code' ,'Mobile Number', 'Room Type','Created At','Sub Task','Status');
    // fputcsv($f, $fields, $delimiter);
    //output each row of the data, format line as csv and write to file pointer
    foreach ($tasks as $key => $task) {
      $area = $task['taskLocation']['address'].', '.$task['location']['area'].', '.$task['location']['city'].' '.$task['location']['zip'];
      $lineData = array('Cleaner Name', $task['user']['name']);
      fputcsv($f, $lineData, $delimiter);
      $lineData = array('Location', $area);
      fputcsv($f, $lineData, $delimiter);
      $lineData = array('Date', date('d/m/Y', strtotime($task['created_at'])));
      fputcsv($f, $lineData, $delimiter);
      $fields = array('Room','Task', 'Subtask' ,'Status', 'Comment');
      fputcsv($f, $fields, $delimiter);
      foreach ($task['subTasks'] as $key => $subTask) {
        if($subTask['status'] == 0)
        {
          $status = 'Pending';
        }
        else if($subTask['status'] == 1)
        {
          $status = 'Issue';
        }
        else
        {
          $status = 'Completed';
        }
        $subTaskData = array(ucfirst($task['room']['room_type']),$task['task_name'], $subTask['task_name'],$status,'comment');
        fputcsv($f, $subTaskData, $delimiter);
      }
      $blank = array('','','','','','','','','');
      fputcsv($f, $blank, $delimiter);
    }
    //move back to beginning of file
    fseek($f, 0);
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    // //output all remaining data on a file pointer
    fpassthru($f);
  }
}
