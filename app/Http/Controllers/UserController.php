<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\User;
use App\Location;
use App\UserLocation;
use App\Task;
use App\SubTask;
use App\Room;
use Mail;
use Hash;
use Session;

class UserController extends Controller
{
  public function index(Request $request)
  {
  	$users = User::with('userLocation.location')->where('role',1)->paginate(10);
    $locations = Location::all();
    $rooms = Room::all();

    $addresses = UserLocation::pluck('address')->toArray();
    //dd($addresses);
    return view('user.index',compact('users','locations','rooms','addresses'));
  }

  public function store(Request $request)
  {
    //dd($request->all());
  	$password = substr($request->email, 0, 3).substr ($request->phone_number, -5);
    $email = $request->email;
  	$user = new User();
  	$user->first_name = ucfirst($request->first_name);
  	$user->last_name = ucfirst($request->last_name);
  	$user->name = ucfirst($request->first_name).' '.ucfirst($request->last_name);
  	$user->email = $email;
  	$user->phone = $request->phone_number;
  	$user->password = Hash::make($password);
  	$user->save();
    foreach ($request->user as $cleaner)
    {
      //dd($cleaner);
      $user_location = new UserLocation();
      $user_location->user_id = $user->id;
      $user_location->location_id = $cleaner['assign_location'];
      /*$user_location->room_id = $cleaner['room_type'];*/
      $user_location->door_unlock_code = $cleaner['door_code'];
      $user_location->contact_person = ucfirst($cleaner['contact_person']);
      $user_location->contact_number = $cleaner['contact_number'];
      $user_location->address = ucfirst($cleaner['address']);
      $user_location->save();
    }
    

      try
      {       
        Mail::send('email.login', ['email' => $email,'password'=> $password], function ($m) use ($email) {
          $m->from(env("MAIL_FROM_ADDRESS", "rajat.kumar@udaantechnologies.info"), 'Cleaning Wizard');

          $m->to($email)->subject('Cleaner Login Password.');
        });
      }
      catch(\Exception $e){
           dd($e);
        return response()->json(['status' => 'Failed','message'=>'Mail not sent'],200);
      } 
    Session::flash('success','Cleaner data created Successfully.');
    return redirect()->route('user.index');
  }

  public function update(Request $request)
  {
  	$user = User::whereId($request->cleaner_id)->first();
    //dd($user);
  	$user->first_name = ucfirst($request->first_name);
  	$user->last_name = ucfirst($request->last_name);
  	$user->name = ucfirst($request->first_name).' '.ucfirst($request->last_name);
  	$user->email = $request->email;
  	$user->phone = $request->phone_number;
  	$user->save();

    $user_location = UserLocation::where('user_id',$request->cleaner_id)->delete();
    foreach ($request->user as $cleaner)
    {
      //dd($cleaner);
      $user_location = new UserLocation();
      $user_location->user_id = $user->id;
      $user_location->location_id = $cleaner['assign_location'];
      /*$user_location->room_id = $cleaner['room_type'];*/
      $user_location->door_unlock_code = $cleaner['door_code'];
      $user_location->contact_person = ucfirst($cleaner['contact_person']);
      $user_location->contact_number = $cleaner['contact_number'];
      $user_location->address = ucfirst($cleaner['address']);
      $user_location->save();
    }

    Session::flash('success','Cleaner data updated Successfully.');
    return redirect()->route('user.index');
  }

  public function delete(Request $request)
  {
    $alltask = Task::where('user_id',$request->cleaner_id)->pluck('id');
    $subtask = SubTask::whereIn('task_id',$alltask)->delete();
    $task = Task::where('user_id',$request->cleaner_id)->delete();
  	$user = User::whereId($request->cleaner_id)->delete();
    $user_location = UserLocation::where('user_id',$request->cleaner_id)->delete();
  	Session::flash('success','Cleaner deleted Successfully.');
    return redirect()->route('user.index');
  }

  public function editPopupData(Request $request)
  {
    $user = User::with('userLocation.location')->whereId($request->cleaner_id)->first();
    $locations = Location::all();
    $rooms = Room::all();
    $edit_addresses = UserLocation::where('user_id','!=',$request->cleaner_id)->pluck('address')->toArray(); //for validation in address field in edit pop-up
    //dd($user);
    return view("user.edit_user_popup",compact('user','locations','rooms','edit_addresses'))->render();
  }

}
