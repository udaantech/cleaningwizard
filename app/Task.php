<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
	protected $fillable = ['user_id','city_area_id','room_id','task_name','address','door_unlock_code','contact_person','contact_number'];
	
    public function room()
    {
        return $this->hasOne('App\Room','id','room_id');
    }
    public function subTasks() {
    	return $this->hasMany('App\SubTask','task_id','id');
    }
    public function user() {
    	return $this->belongsTo('App\User');
    }
    public function location() {
        return $this->hasOne('App\Location','id','city_area_id');   
    }
    public function taskLocation() {
        return $this->belongsTo('App\UserLocation','address','id');
    }
    
}
