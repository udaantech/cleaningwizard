<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Mail;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userLocation()
    {
        return $this->hasMany('App\UserLocation','user_id','id');
    }

    public function task()
    {
        return $this->hasMany('App\Task','user_id','id');
    }

    public function sendPasswordResetNotification($token){
    // $this->notify(new MyCustomResetPasswordNotification($token)); <--- remove this, use Mail instead like below

    $data = [
        $this->email
    ];

    Mail::send('email.reset', [
        'fullname'      => $this->fullname,
        'reset_url'     => route('password.reset', ['token' => $token, 'email' => $this->email]),
    ], function($message) use($data){
        $message->subject('Reset Password Request');
        $message->to($data[0]);
    });


}
}
