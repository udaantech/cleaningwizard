<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLocation extends Model
{
	protected $fillable = [
        'user_id', 'location_id',
    ];
    public function location()
    {
        return $this->hasOne('App\Location','id','location_id');
    }
    public function users()
    {
        return $this->belongsTo('App\User','user_id', 'id')->select('id','name');
    }
    public function roomType()
    {
        return $this->belongsTo('App\Room','room_id', 'id')->select('id','room_type');
    }
}
