@if (session($key ?? 'status'))
    <div class="alert alert-success" role="alert">
        {{ session($key ?? 'status') }}
    </div>
@endif
@if (session($key ?? 'errors'))
    <div class="alert alert-danger" role="alert">
        {{ session($key ?? 'errors') }}
    </div>
@endif

