@extends('layouts.app', ['class' => 'login-page', 'page' => __('Login Page'), 'contentClass' => 'login-page'])

@section('content')

 
<div class="login-background-img">
    <div class="row justify-content-center">
        <div class="col-lg-5 ">
            <div class="card card-white">
                <div class="card-header"><img src="{{ asset('black') }}/images/login-logo.png"> </div>
                <div class="card-body">
        <form class="form" method="post" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                
                    
                    <label for="email" class="col-md-12 col-form-label text-md-left">Sign in</label>
                    <div class="cw-form-box-div {{ $errors->has('email') ? ' has-danger' : '' }}">
                        <input class="form-control  {{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" name="email" placeholder="Email">
                        <span class="cw-form-box-focus-input"></span>
                        <span class="cw-form-box-symbol-input">
                            <img src="{{ asset('black') }}/images/Email.png">
                            <!-- <i class="fa fa-envelope" aria-hidden="true"></i> -->
                        </span>
                        @include('alerts.feedback', ['field' => 'email'])
                    </div>
                </div>
                <div class="form-group">
                    <div class="cw-form-box-div {{ $errors->has('password') ? ' has-danger' : '' }}">
                        <input class="form-control  {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" placeholder="Password">
                        <span class="cw-form-box-focus-input"></span>
                        <span class="cw-form-box-symbol-input">
                            <img src="{{ asset('black') }}/images/Password.png">
                            <!-- <i class="fa fa-lock" aria-hidden="true"></i> -->
                        </span>
                        @include('alerts.feedback', ['field' => 'password'])
                    </div>
                    <!-- Ankit -->
                </div>
              <a href="#">
                <div class=" cw-logo-page-footer">
                    <button type="submit" class="cw-logo-page-footer-btn">{{ __('Log in') }}</button>
                    <!-- <div class="pull-left">
                        <h6>
                            <a href="{{ route('register') }}" class="link footer-link">{{ __('Create Account') }}</a>
                        </h6>
                    </div>
                     -->

                </div> 
            </a>
            </form>
                 <div class="pull-right footer-button-forgot">
          <h6>
            <a href="{{ route('password.request') }}" class="link footer-link">{{ __('Forgot password?') }}</a>
          </h6>
        </div>
            </div>

        </form>
       
    </div>
    </div>
</div>
@endsection
