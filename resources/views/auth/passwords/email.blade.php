 
@extends('layouts.app', ['class' => 'login-page', 'page' => __('Login Page'), 'contentClass' => 'login-page'])
@section('content')


      
        <div class="login-background-img">
    <div class="row justify-content-center">
        <div class="col-lg-5 ">
            <div class="card card-white">
                <div class="card-header"><img src="{{ asset('black') }}/images/login-logo.png"> </div>
                <!-- {{ __('Reset Password') }} -->

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email" class="col-md-12 col-form-label text-md-left">{{ __('Forgot Password') }}</label>

                            
                            <div class="col-md-12 cw-form-box-div">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror cw-form-box-forgot" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email" autofocus>

                                <span class="cw-form-box-focus-input"></span>
                        <span class="cw-form-box-symbol-forgot">
                            <img src="{{ asset('black') }}/images/Email.png">
                            <!-- <i class="fa fa-envelope" aria-hidden="true"></i> -->
                        </span>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <!--  -->

                            <!-- <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div> -->
                            <!--  -->
                        </div>

                        <div class="form-group for-forget-password-button">
                            <div class="reset-password">
                                <button type="submit" class=" reset-password-btn">
                                    {{ __('Reset Password') }}
                                </button>
                                <a type="button" href="{{url('/login')}}" class="reset-password-Sign">{{ __('Sign in!') }}</a>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
