<!DOCTYPE html>
<html>
   <head>
      <title>Cleaning Wizard</title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
   </head>
   <body>
      <div class="mail-temp-box">
         <table class="table task-table" style="width: 100%; margin:0 auto; display: block; background: none;     display: table; border-spacing: 0; border-collapse: collapse; table-layout: fixed;">
            <tbody style="width: 100%;">
               <tr>
                  
               </tr>
               <tr>
                  <td style="padding: 10px 20px;  background: #fff;  width: 100%;  font-weight: 400; font-family: -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif;">
                     <h4 style="font-family: -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif;  margin: 10px 0; font-size: 16px; color: #000; font-weight: 700;">Hey,</h4>
                     <p style="font-family: -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif; font-size: 14px; color: #000; font-weight: 400; line-height: 22px; margin: 12px 0;"></p>
                     <p style="     font-family: -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif; margin: 30px 0 0;font-size: 16px; color: #000; font-weight: 600;">{{$email}}</p> 
                  </td>
               </tr>
              
               <tr style="margin-top: 20px;">
                  <td style=" padding: 0 20px 20px 20px; background: #1b282e;  width: 100%;  text-align: center; font-weight: 400; font-family: -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif; " >
                     <h3 style="font-family: -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif;  margin: 10px 0; font-size: 16px; color: #fff; font-weight: 600;padding: 20px 0 0;">Your credentials are:</h3>
                     <h4 style="color: white;">Email Id: </h4> <h4 style="color: #598FDE;"> {{$email}}</h4>
               <h4 style="color: white;">Password: </h4> <h4 style="color: #598FDE;"> {{$password}}</h4> 
                  </td>
               </tr>
               
            </tbody>
         </table>
      </div>
   </body>
</html>