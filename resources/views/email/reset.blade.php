<!DOCTYPE html>
<html>
  <head>
    <title></title>
  </head>
  <body style="margin: 0px; padding: 0px; box-sizing: border-box;">
    <div style="width: 210mm; text-align: left; height: 297mm;margin: 15px auto; background: #fff;/*border: 1px solid #f1f0f0;*/display: block;font-size: 12px; font-family: 'Roboto', sans-serif;">
      <div >
        <table width="100%">
          <tbody>
            <tr>
            <td style="/*font-size: 50px;letter-spacing:5px;font-family: Verdana, Arial, Helvetica, sans-serif;color:#005B9A;*/text-align: center;width: 100%;padding: 0px 0px 40px 0px"> <img src="{{asset('images/bbb.png')}}" style="/*float: right;*/width: 65%;">
            </td>
          </tr>
        </tbody>
      </table>
      <table width="100%" >
        <tbody>
          <tr>
            <td style="text-align: center;width: 100%;"> <img src="{{asset('images/ffff.png')}}" style="width: 100%;">
            </td>
        </tr>
        <tr>
          <td style="font-size: 50px;letter-spacing: 0px;font-family:'Roboto', sans-serif;color: #000;text-align: center;width: 100%;font-weight: 800;padding: 34px 0px 15px 0px; line-height: 50px;
">Password Reset
          </td>
        </tr>
        <!-- <tr>
          <td style="font-size: 20px;letter-spacing: 1px;font-family:'Roboto', sans-serif;color: #000;text-align: center;width: 100%;font-weight: 700;">Email: <a href="#">abc@cleaningwized.com</a>, password: <a href="#">123456</a> 
          </td>

        </tr> -->
        <tr>
          <td style="font-size: 20px;letter-spacing: 1px;font-family:'Roboto', sans-serif;color: #000;text-align: center;width: 100%;font-weight: 600;padding: 30px 80px 50px 80px;">Seems like forgot your password for Logo Inc. If this is true, click below to reset your password. 
          </td>
          
        </tr>
        <tr>
        <td style="text-align: center;width: 100%;font-weight: 400;padding: 23px; ">
          <a href="{{$reset_url}}" style="background-color: #5300cb;font-size: 30px;letter-spacing: 1px;font-family:'Roboto', sans-serif;color: #fff;text-align: center;width: 100%;font-weight: 400;padding: 23px; cursor: pointer!important; border-radius: 5px;">Reset My Password</a>
        </td>
      </tr>
          <tr>
          <td style="font-size: 20px;letter-spacing: 1px;font-family:'Roboto', sans-serif;color: #808080;text-align: center;width: 100%;font-weight: 500;padding: 34px 0px 50px 0px">If you did not forgot your password you can safely ignore this email. 
          </td>
          
        </tr>
        <tr style="display: block; background-color: #f2f2f2; width: 85%; margin: auto; text-align: -webkit-center;">
          <td style="font-size: 20px;letter-spacing: 1px;font-family:'Roboto', sans-serif;color: #000;text-align: center;width: 100%;font-weight: 500;padding: 30px; ">Need more helps?<br> <a style="text-decoration: none;" href="#">we're here, ready to talk</a> 
          </td>
          
        </tr>
        <tr>
            <td style="text-align: center;width: 100%;padding: 25px 0px;"> <img src="{{asset('images/bbb.png')}}" style="width: 50%;">
            </td>
          </tr>
          <tr style="display: block; background-color: #000;color: #fff; width: 100%; margin: auto; text-align: -webkit-center;">
          <td style="font-size: 14px;letter-spacing: 1px;font-family: 'Roboto', sans-serif;text-align: center;width: 100%;font-weight: 500;padding: 30px;">Copyright @2020 Cleaning Wizard. All Rights Reserved 
          </td>
          
        </tr>
      </tbody>
    </table>
  </div>
</div>
</body>
</html>