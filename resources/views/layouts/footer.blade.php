<footer class="footer">
    <div class="container-fluid">
        <ul class="nav">
            <li class="nav-item">
                <a href="https://udaantechnologies.com" target="blank" class="nav-link">
                    {{ __('Udaan Technologies Pvt Ltd') }}
                </a>
            </li>
        </ul>
        <div class="copyright">
            &copy; {{ now()->year }}
        </div>
    </div>
</footer>
