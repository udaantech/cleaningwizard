<header class="header">
	<div class="row">
		<div class="col-md-2">
			<div class="logo_part">
        <div class="menu-icon"> 
        
         <a href="#" class="brand-link logo-side-menu-nav-bar-box">
            <img src="{{ asset('images/clean-w-logo2.png') }}">
         </a>
         <a href="#" id="mobile-indicator" class="button-menu float-right"><span class="fa fa-fw fa-bars "></span></a>
       </div>
      </div> 
		</div>
     <?php
      $path = request()->path();
      //dd($path);
     ?>
	 <div class="col-md-10">
	 	<div class="tp-heading">{{ ($path == 'tasks' ? 'Task Management' : ($path == 'users' ? 'User Management' : ($path == 'locations' ? 'Location/Area Management' : ($path == 'rooms' ? 'Room Management' : ($path == 'task/addNewTask' ? 'Task Management' : ($path == 'profile' ? 'Profile' : ''))))))}}</div>
    <nav class="navbar navbar-toggleable-md navbar-light mt-3 pb-0 float-right">
        <div class="menu-section-with-logo"> 
        			
             <div class="user-login-dashboard">
			 
				    <div class="responsive-setting-toggle-cont">
		          <ul class="nav "> 
		            <div class="pull-right">
				        	<li class="dropdown">Welcome <span class="billing">{{Auth::user()->name}}</span>:<a style="cursor:pointer;" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ asset('images/user-icon.png') }}"> </a>
				            	<ul class="dropdown-menu">
				                <!-- <li><a href="/user/preferences"><i class="icon-cog"></i> Preferences</a></li>
				                <li><a href="/help/support"><i class="icon-envelope"></i> Contact Support</a></li> 
				                <li class="divider"></li>-->
				                <li><a href="{{ route('profile.edit') }}"><i class="icon-off"></i> Profile</a></li>
				                <li><a href="{{ route('logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();">{{ __('Log out') }}</a></li>
			                </ul>
			              </li>
				     		</div>
		          </ul>
						</div>
             </div>
        </div>
    </nav>
						</div>
             </div>
</header>