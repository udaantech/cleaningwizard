<div class="sidebar" id="sidebar-menu">
	

	<nav>
		<ul>
			<li><a href="{{route('location.index')}}" class="{{ Request::routeIs('location.index') ? 'active' : '' }}"><img src="{{ asset('images/location.png') }}"> Location/Area</a></li>
			<li ><a href="{{route('user.index')}}" class="{{ Request::routeIs('user.index') ? 'active' : '' }}"><img src="{{ asset('images/user.png') }}"> Users</a></li>
			<li><a href="{{route('room.index')}}" class="{{ Request::routeIs('room.index') ? 'active' : '' }}"><img src="{{ asset('images/room-type.png') }}"> Room Types</a></li>
			<li><a href="{{route('task.index')}}" class="{{ Request::routeIs('task.index') ? 'active' : '' }}"><img src="{{ asset('images/task.png') }}"> Tasks</a></li>
			<li><a href="{{ route('logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();"><img src="{{ asset('images/logout.png') }}"> {{ __('Log out') }}</a></li>
		</ul>
	</nav>
</div>