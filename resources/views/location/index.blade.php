@extends('layouts.app')

@section('content')
<div class="row screen-location-page scrennn-update-page">
  <div class="col-md-12">
    <div class="top-whit-bg ">
 
	<div class=" page-header-section">
      <div class="icon-heading"><span><img src="{{ asset('images/location1.png') }}" class="add-location"></span><p>{{ __('Location/Area') }}</p></div>

      <div class="button-set">  
        <button class="btn btn-add-task"  data-toggle="modal" data-target="#add_location_model">Add New Location</button>
      </div>         
   </div>
<div class="search-filter locatin-filter search-filter-bycity">
     <select name="cities" class="form-control" id="cities">
       <option value="">By City</option>
       @foreach($cities as $location_city)
       <option value="{{$location_city['city']}}" {{request()->query('city') === $location_city['city'] ? 'selected' : ''}} >{{$location_city['city']}}</option>
       @endforeach
     </select>
     <select name="zip_code" class="form-control ddt" id="zip_code">
       <option value="">Zip Code</option>
       @foreach($all_zip as $location_zip)
       <option value="{{$location_zip['zip']}}" {{request()->query('zip') === $location_zip['zip'] ? 'selected' : ''}}>{{$location_zip['zip']}}</option>
       @endforeach
     </select>
     <button class="btn apply form-control-apply-delete" id="apply"> Apply</button></a>
     <a href="#" id="reset"><button class="form-control-reset">Reset</button></a>
   </div>

	<div class="location_listing tabledata">
	  <table class="table-striped">
	    <thead>
	      <tr>
	        <th>City with Area</th>
	        <th>Zip</th>
	        <th>Curfew Time</th>
	        <th>Action</th>
	      </tr>
	    </thead>
	    <tbody>
        @if(count($locations) > 0)
	      @foreach($locations as $location)
	      <tr>
	        <td>{{$location->city}}/{{$location->area}}</td>
	        <td>{{$location->zip}}</td>
	        <td>{{$location->curfew_time}}</td>
	        <td class="action">
	          <a class="edit_popup_data margin-right-20" href="JavaScript:Void(0);" data-toggle="modal" data-target="#edit_location_model" data-id="{{$location->id}}" data-city="{{$location->city}}" data-area="{{$location->area}}" data-zip="{{$location->zip}}" data-curfew="{{$location->curfew_time}}"><img src="{{asset('images/edit.png')}}">Edit</a>
	          <a class="delete-link" href="#" data-toggle="modal" data-target="#deletemodal" data-id="{{$location->id}}"><img src="{{asset('images/delete.png')}}">Delete</a>
	        </td>
	      </tr>
	      @endforeach
        @else
        <tr>
          <td colspan="8" style="text-align: center;">No Data Found.</td>
        </tr>
        @endif
	    </tbody>
	  </table>
    {{ $locations->links() }}
	</div>
</div>
</div>
</div>

<!-- Add New Cleaner Modal -->
<div class="modal fade" id="add_location_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="{{route('location.store')}}" id="add_location_form">
      @csrf
      <div class="Modal-Header">
        <h3>Add New Location</h3> <a class="modal-cancel-btn" data-dismiss="modal">X</a>
      </div>
      <div class="Modal-body">
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <div class="form-group">
              <label>City</label>
              <input class="form-control" type="text" name="city" id="city">
              <span class="city_err" style="color: red;"></span>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="form-group">
              <label>Location Details</label>
              <input class="form-control" type="text" name="area" id="area">
              <span class="area_err" style="color: red;"></span>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="form-group">
              <label>Zip</label>
              <input class="form-control" type="text" name="zip" id="zip">
              <span class="zip_err" style="color: red;"></span>
            </div>
          </div>
          <div class="col-md-12">
          <div class="row">
          <div class="col-md-5 col-sm-12"> 
                <label>Curfew Time</label>
              <div class="input-group">
                <select class="form-control" name="start_time" id="start_time">
                  <option value="">Select One</option>
                  @foreach($hours as $hour)
                  <option value="{{$hour}}">{{$hour}}</option>
                  @endforeach
                </select>
                <!-- <span class="start_time_err" style="color: red;"></span>  -->
                <select class="form-control" name="start_ampm" id="start_ampm">
                  <option value="">Select One</option>
                  <option value="AM">AM</option>
                  <option value="PM">PM</option>
                </select>
              </div>
              <span class="start_time_err" style="color: red;"></span> 
              
            </div>
          <div class="col-md-2">
             <label class="margin-top-20"> </label>
              <div class="text-center">To</div>           
          </div>
          <div class="col-md-5 col-sm-12"> 
            <label> </label>
            <div class="input-group">
              <select class="form-control" name="end_time" id="end_time">
                <option value="">Select One</option>
                @foreach($hours as $hour)
                <option value="{{$hour}}">{{$hour}}</option>
                @endforeach
              </select>
              <select class="form-control" name="end_ampm" id="end_ampm">
                <option value="">Select One</option>
                <option value="AM">AM</option>
                <option value="PM">PM</option>
              </select>
              <!-- <span class="end_time_err" style="color: red;"></span> -->
            </div> 
            <span class="end_time_err" style="color: red;"></span>
          </div>
          </div>
          </div>

          </div>
      </div>
      <div class="Modal-footer text-right">
        <button type="button" class="btn modal-cancel-btn btn-export" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn modal-save-btn btn-add-task margin-left-15" id="submit">Save</button>
      </div> 
      </form>
    </div>
  </div>
</div> 
<!-- Edit Cleaner Modal -->
<div class="modal fade" id="edit_location_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="{{route('location.update')}}" id="edit_location_form">
      @csrf
      <input type="hidden" name="location_id" id="location_id">
      <div class="Modal-Header">
        <h3>Edit Location</h3><a class="modal-cancel-btn" data-dismiss="modal">X</a>
      </div>
      <div class="Modal-body">
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <div class="form-group">
              <label>City</label>
              <input class="form-control" type="text" name="city" id="city1">
              <span class="city_err1" style="color: red;"></span>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="form-group">
              <label>Location Details</label>
              <input class="form-control" type="text" name="area" id="area1">
              <span class="area_err1" style="color: red;"></span>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="form-group">
              <label>Zip</label>
              <input class="form-control" type="text" name="zip" id="zip1">
              <span class="zip_err1" style="color: red;"></span>
            </div>
          </div> 
          <div class="col-md-12">
          <div class="row">
          <div class="col-md-5 col-sm-12">
              <label>Curfew Time</label>
            <div class="input-group">
              <select class="form-control" name="start_time" id="start_time1">
                <option value="">Select One</option>
                @foreach($hours as $hour)
                <option value="{{$hour}}">{{$hour}}</option>
                @endforeach
              </select>
              <!-- <span class="start_time_err1" style="color: red;"></span> -->
              <select class="form-control" name="start_ampm" id="start_ampm1">
                <option value="">Select One</option>
                <option value="AM">AM</option>
                <option value="PM">PM</option>
              </select>

            </div>
            <span class="start_time_err1" style="color: red;"></span>
          </div>
          <div class="col-md-2 col-sm-12">
            <label class="margin-top-20"> </label>
              <div class="text-center">To</div>
          </div>
          <div class="col-md-5 col-sm-12">
            <label> </label>
            <div class="input-group">
              <select class="form-control" name="end_time" id="end_time1">
                <option value="">Select One</option>
                @foreach($hours as $hour)
                <option value="{{$hour}}">{{$hour}}</option>
                @endforeach
              </select>
              <select class="form-control" name="end_ampm" id="end_ampm1">
                <option value="">Select One</option>
                <option value="AM">AM</option>
                <option value="PM">PM</option>
              </select>
              <!-- <span class="end_time_err1" style="color: red;"></span> -->
            </div>
            <span class="end_time_err1" style="color: red;"></span>
          </div>
      </div>
    </div>
      </div>
    </div>
      <div class="Modal-footer text-right">
        <button type="button" class="btn  modal-cancel-btn btn-export" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn  modal-save-bt btn-add-task" id="submit">Save</button>
      </div>
      </form>
      </div>
    </div>
  </div>

<!-- Delete model -->
<div class="modal fade alert-popup" id="deletemodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="Modal-Header Modal-Header-display-delete-record">
         <h5>Delete Record</h5>
        <a class="close close-delete-record" data-dismiss="modal" aria-label="Close">X</a>
      </div>
      <form action="{{route('location.delete')}}" method="post">
        @csrf
       <div class="Modal-body Modal-body-display-delete-record">
        <input type="hidden" name="location_id" id="delete-id">  
        Do you want to Delete Location/Area?
      </div>
     <div class="Modal-footer text-right text-right-display-delete-record">
        <button type="button" class="btn btn-export btn-export-display-delete-record" data-dismiss="modal">No</button>
        <button  type="submit" class="btn btn-add-task btn-export-display-delete-record">Yes</button>
      </div>
    </form>
    </div>
  </div>
</div>
<script type="text/javascript">

  $('.location_listing').on('click','.edit_popup_data',function(){
  $('#location_id').val($(this).data('id'));
  $('#city1').val($(this).data('city'));
  $('#area1').val($(this).data('area'));
  $('#zip1').val($(this).data('zip'));
  $('#curfew1').val($(this).data('curfew'));
  var curfew = $(this).data('curfew').split(" ");
  $('#start_time1').val(curfew[0]);
  $('#start_ampm1').val(curfew[1]);
  $('#end_time1').val(curfew[3]);
  $('#end_ampm1').val(curfew[4]);
  });

  $('.location_listing').on('click','.delete-link',function(){
    $('#delete-id').val($(this).data('id'));
  });

</script>
<script type="text/javascript">
    $('#add_location_form').submit(function(e) {

    var city = $('#city').val();
    if(city.length == 0)
    {
      e.preventDefault();
      $('.city_err').text('City is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }
    var area = $('#area').val();
    if(area.length == 0)
    {
      e.preventDefault();
      $('.area_err').text('Area is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }
    var zip = $('#zip').val();
    if(zip.length == 0)
    {
      e.preventDefault();
      $('.zip_err').text('Zip is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }
    var start_time = $('#start_time').val();
    var end_time = $('#end_time').val();
    var start_ampm = $('#start_ampm').val();
    var end_ampm = $('#end_ampm').val();
    if(start_time.length == 0 || start_ampm.length == 0)
    {
      e.preventDefault();
      $('.start_time_err').text('Curfew start time is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }

    if(end_time.length == 0 || end_ampm.length == 0)
    {
      e.preventDefault();
      $('.end_time_err').text('Curfew end time is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }
    
    /*var curfew_time = $('#curfew_time').val();
    if(curfew_time.length == 0)
    {
      e.preventDefault();
      $('.curfew_time_err').text('Curfew time is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }*/

    });
       
 </script>
 <script type="text/javascript">
    $('#edit_location_form').submit(function(e) {

    var city = $('#city1').val();
    if(city.length == 0)
    {
      e.preventDefault();
      $('.city_err1').text('City is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }
    var area = $('#area1').val();
    if(area.length == 0)
    {
      e.preventDefault();
      $('.area_err1').text('Area is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }
    var zip = $('#zip1').val();
    if(zip.length == 0)
    {
      e.preventDefault();
      $('.zip_err1').text('Zip is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }
    var start_time = $('#start_time1').val();
    var end_time = $('#end_time1').val();
    var start_ampm = $('#start_ampm1').val();
    var end_ampm = $('#end_ampm1').val();
    if(start_time.length == 0 || start_ampm.length == 0)
    {
      e.preventDefault();
      $('.start_time_err1').text('Curfew start time is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }

    if(end_time.length == 0 || end_ampm.length == 0)
    {
      e.preventDefault();
      $('.end_time_err1').text('Curfew end time is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }

    });
       
 </script>

 <script type="text/javascript">
   $('#apply').on('click',function(){
     var city = $('#cities').val();
     var zip = $('#zip_code').val();
     
    window.location.href = "{{url('locations')}}?city="+city+'&zip='+zip;
   });
   $("#reset").on('click',function(){ 
    window.location.href = "{{url('locations')}}?city=&zip=";
   })
 </script>


@endsection
