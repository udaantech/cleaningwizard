@extends('layouts.app', ['page' => __('Location Management'), 'pageSlug' => 'product'])

@section('content')
<div class="row screen-location-page member-ship-page  Add-Locations-page">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header header-section-manage-location">
        <div class="row">
          <div class="col-8">
            <h2 class="card-title"><img src="{{ asset('black') }}/img/Location-icon.png" class="add-location"> {{ __('Add Locations') }}</h2>
          </div>
        </div>
      </div>
      <div class="card-body">
        @include('alerts.success')
        <div class="row">
          <div class="col-12 manage-border">
            <h4 class="card-title heading-member-color">{{ $product_name }}</h4>
          </div>
        </div>
        
        <div class="row activeinactive add-location-page-screen">
          <div class="col-6 form-group active-group input-member-plan">
            <i class="fa fa-search" aria-hidden="true"></i>
            <input type="text" name="search_location" id="search_location" class="form-control form-control-alternative" placeholder="{{ __('Search Location..') }}" value="{{ old('search_location') }}">
          </div>
            <div class="col-md-6 active-group MEMBERSHIP-plan">
              <div class="form-group">
                <input type="radio" id="test3" name="location_status" class="l location_status" value="Active" checked="checked">
                <label for="test3">Active</label><span class="button-border">|</span>
                <input type="radio" id="test4" name="location_status" class="location_status" value="Inactive"> 
                <label for="test4">Inactive</label><span class="button-border">|</span>  
                 <button type="submit" class="btn btn-success btn-search mt-4">{{ __('SEARCH') }}</button>
              </div>
            </div> 
        </div>

        <div class="table-screen-page manage-border-mamber">
          <form method="post" action="{{ route('product.store') }}">
            @csrf
            <input type="hidden" name="product_id" value='{{ $product_id }}'>
            <div class="table-responsive">
              <table class="table tablesorter add-location-table" id="">
                <thead class=" text-primary">
                  <th >{{ __('LOCATION') }}</th>
                  <th ></th>
                
                </thead>
                <tbody id="active-locations-list" class="table-location"></tbody>
                <tbody id="inactive-locations-list" class="table-location" ></tbody>
              </table>
            </div>
            <div class="text-Left">
              <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
              <a  href="{{ route('product.index') }}" type="button" class="btn btn-danger mt-4">{{ __('Cancel') }}</a>
            </div>
          </form>
        </div>
      </div>  
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#active-locations-list").css("display", "block");
  $("#inactive-locations-list").css("display", "none");
  var map_location_id = @json($map_location_id);
  var inactive_loc_list = @json($inactive_location_list); 
  var active_loc_list = @json($active_location_list); 
  var active_location = "";
  var inactive_location = "";
  $.each(active_loc_list, function (key, val) {
    active_location +="<tr><td class='form-group-checkbox'><input type='checkbox' id='"+val.id+"'  class='location' name='location_ids[]' value='"+val.id+"' ><label for='"+val.id+"'></label></td><td><b>"+val.center_name+"</b> <br>"+val.address+", "+val.city+", "+val.state+"</td></tr>";
  });
  $.each(inactive_loc_list, function (key, val) {
    inactive_location +="<tr><td class='form-group-checkbox'><input type='checkbox' id='"+val.id+"' class='location' name='location_ids[]' value='"+val.id+"'><label for='"+val.id+"'></label></td><td><b>"+val.center_name+"</b> <br>"+val.address+", "+val.city+", "+val.state+"</td></tr>";
  });  

  $("#active-locations-list").html(active_location);
  $("#inactive-locations-list").html(inactive_location);

  $(".location_status").click(function(e){
    var radioValue = $("input[name='location_status']:checked").val();
    if(radioValue=="Active"){
      $("#active-locations-list").css("display", "block");
      $("#inactive-locations-list").css("display", "none");
    }else{
      $("#active-locations-list").css("display", "none");
      $("#inactive-locations-list").css("display", "block"); 
    }
  });

  $(".btn-search").click(function(e){
    var searchValue = $("#search_location").val();
    searchValue =  new RegExp(searchValue,'gi');
    var radioValue = $("input[name='location_status']:checked").val();
    if(radioValue=="Active"){
      var search_act_loc ="";
      console.log(searchValue);
      $.each(active_loc_list, function (key, val) {
        if (val.center_name.match(searchValue)) {
          search_act_loc +="<tr><td class='form-group-checkbox'><input type='checkbox' id='"+val.id+"'  class='location' name='location_ids[]' value='"+val.id+"' ><label for='"+val.id+"'></label></td><td><b>"+val.center_name+"</b> <br>"+val.address+", "+val.city+", "+val.state+"</td></tr>";
        }
      });
      $("#active-locations-list").html(search_act_loc);
    }else{
      var search_inact_loc ="";
      $.each(inactive_loc_list, function (key, val) {
        if (val.center_name.match(searchValue)) {
        search_inact_loc +="<tr><td class='form-group-checkbox'><input type='checkbox' id='"+val.id+"' class='location' name='location_ids[]' value='"+val.id+"'><label for='"+val.id+"'></label></td><td><b>"+val.center_name+"</b> <br>"+val.address+", "+val.city+", "+val.state+"</td></tr>"; 
        }
      });
      $("#inactive-locations-list").html(search_inact_loc);
    }
    $.each(map_location_id, function (key, val) {
      $("#"+val.location_id).prop( "checked", true );
    });
  });

  $.each(map_location_id, function (key, val) {
    $("#"+val.location_id).prop( "checked", true );
  });

</script>
@endsection
