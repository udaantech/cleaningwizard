@extends('layouts.app', ['page' => __('Location Management'), 'pageSlug' => 'product'])

@section('content')
  <div class="container-fluid mt--7">
    <div class="row">
      <div class="col-xl-12 order-xl-1">
        <div class="card">
          <div class="card-header">
            <div class="row align-items-center">
              <div class="col-8">
                <h3 class="mb-0">{{ __('Locations') }}</h3>
              </div>
              <div class="col-4 text-right">
                <a href="{{ route('location.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <form method="post" action="{{ route('location.update', $location) }}" autocomplete="off">
              @csrf
              @method('put')

            <h6 class="heading-small text-muted mb-4">{{ __('Location information') }}</h6>
            <div class="pl-lg-4">
                <div class="form-group{{ $errors->has('center_name') ? ' has-danger' : '' }}">
                  <img src="{{ asset('black') }}/img/center-name.png" class="add-location-form">
                  <label class="form-control-label" for="input-center_name">{{ __('Center Name') }}</label>
                  <input type="text" name="center_name" id="input-center_name" class="form-control form-control-alternative{{ $errors->has('center_name') ? ' is-invalid' : '' }}" placeholder="{{ __('Center Name') }}" value="{{ old('center_name', $location->center_name) }}" required autofocus>
                  @include('alerts.feedback', ['field' => 'center_name'])
                </div>
                <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                  <img src="{{ asset('black') }}/img/state.png" class="add-location-form">
                  <label class="form-control-label" for="input-address">{{ __('Address') }}</label>
                  <input type="text" name="address" id="input-address" class="form-control form-control-alternative{{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="{{ __('Address') }}" value="{{ old('address', $location->address) }}" required>
                  @include('alerts.feedback', ['field' => 'address'])
                </div>
                <div class="form-group{{ $errors->has('city') ? ' has-danger' : '' }}">
                  <img src="{{ asset('black') }}/img/state.png" class="add-location-form">
                  <label class="form-control-label" for="input-city">{{ __('City') }}</label>
                  <input type="text" name="city" id="input-city" class="form-control form-control-alternative{{ $errors->has('city') ? ' is-invalid' : '' }}" placeholder="{{ __('City') }}" value="{{ old('city', $location->city) }}" required>
                  @include('alerts.feedback', ['field' => 'city'])
                </div>
                <div class="form-group{{ $errors->has('state') ? ' has-danger' : '' }}">
                  <img src="{{ asset('black') }}/img/state.png" class="add-location-form">
                  <label class="form-control-label" for="select-state">{{ __('State') }}</label>
                  <select name="state" id="input-state" class="form-control form-control-alternative{{ $errors->has('state') ? ' is-invalid' : '' }}" >
                  <option value=''>Select State</option> 
                  @foreach ($state_list as $list)  
                    @if($location->state == $list)
                    <option value='{{ $list }}' selected >{{ $list }}</option>
                    @else
                     <option value='{{ $list }}' >{{ $list }}</option>
                    @endif
                  @endforeach
                  </select>    
                    @include('alerts.feedback', ['field' => 'state'])
                </div>
                <div class="form-group{{ $errors->has('pincode') ? ' has-danger' : '' }}">
                  <label class="form-control-label" for="input-pincode">{{ __('Pincode') }}</label>
                  <input type="text" name="pincode" id="input-pincode" class="form-control form-control-alternative{{ $errors->has('pincode') ? ' is-invalid' : '' }}" placeholder="{{ __('Pincode') }}" value="{{ old('pincode', $location->pincode) }}" >
                  @include('alerts.feedback', ['field' => 'pincode'])
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
