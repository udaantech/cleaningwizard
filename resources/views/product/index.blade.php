@extends('layouts.app', ['page' => __('Location Management'), 'pageSlug' => 'product'])

@section('content')  
<div class="row screen-location-page product-page-screen">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header header-section-manage-location">
        <div class="row">
          <div class="col-8">
            <h4 class="card-title "><img src="{{ asset('black') }}/img/product.png" class="add-location">{{ __('Products') }}</h4>
          </div>
          <div class="col-4 text-right">
            
          </div>
        </div>
      </div>
      <div class="card-body">
        @include('alerts.success')
        <div class="row">
          <div class="col-12 manage-border">
            <h4 class="card-title product-heading">{{ __('PRODUCTS LIST') }}</h4>
          </div>
        </div>
        <div class="table-screen-page">
          <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <th class="product-table" scope="col">{{ __('Product Name') }}</th>
              <th class="location-table" scope="col">{{ __('Locations') }}</th>
              <th class="action-table" scope="col">{{ __('Action') }}</th>
            </thead>
            <tbody>
              @foreach ($shopify_products as $key=>$product)
              
              <tr>
                <td>{{ $product->title }}</td>
                <td class="product-location-details"> 
                  @if(isset($product->location_details))
                    @foreach ($product->location_details as $location)
                    <form class="details-form" action="{{ route('product.destroy', $location->location_id) }}?product_id={{$product->id}}" method="post">
                    @csrf
                    @method('delete')
                    {{$location->center_name}}
                    <a  title='Delete Mapped Location' class="tim-icons icon-trash-simple delet-icon" data-toggle='tooltip' onclick="confirm('{{ __("Are you sure you want to delete this Mapped Location?") }}') ? this.parentElement.submit() : ''"><i></i></a>
                    </form>
                    @endforeach
                  @endif
                </td>
                <td class="add-product-table"> 
                   <a  href="{{ route('product.create') }}?product_id={{$product->id}}&product_name={{$product->title}}"  title='Add Product' data-toggle='tooltip'><img src="{{ asset('black') }}/img/Location-icon.png" class="add-location"></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        </div>
      </div>
      <div class="card-footer py-4">
        <nav class="d-flex justify-content-end" aria-label="...">
       
        </nav>
      </div>

    </div>
  </div>
</div>
@endsection

