@extends('layouts.app', ['page' => __('User Profile'), 'pageSlug' => 'profile'])

@section('content')
    <div class="row admin-profile-page">
        <div class="col-md-6">
            <div class="card admin-profile-page-card-clean">
                <div class="card-header">
                    <h5 class="title">{{ __('Edit Profile') }}</h5> 
                </div>
                <form method="post" action="{{ route('profile.update') }}" autocomplete="off" id="edit-profile">
                    <div class="card-body">
                            @csrf
                            @method('put')

                            <div class="form-group">
                                <label>{{ __('Name') }}</label>
                                <input type="text" name="name" id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Name') }}" value="{{ old('name', auth()->user()->name) }}">
                                @include('alerts.feedback', ['field' => 'name'])
                                 <span class="name_err" style="color: red;"></span>
                            </div>
                           <!--  <span class="name_err" style="color: red;"></span> -->
                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                <label>{{ __('Email address') }}</label>
                                <input type="email" name="email" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Email address') }}" value="{{ old('email', auth()->user()->email) }}">
                                @include('alerts.feedback', ['field' => 'email'])
                                <span class="email_err" style="color: red;"></span>
                            </div>
                            <!-- <span class="email_err" style="color: red;"></span> -->
                    </div>
                    <br><br><br>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary">{{ __('Update') }}</button>
                    </div>
            
                </form>
            
            
            </div>

        </div>    
        <div class="col-md-6 edit-form">    
            <div class="card">
                <div class="card-header">
                    <h5 class="title">{{ __('Change Password') }}</h5>
                </div>
                <form method="post" action="{{ route('profile.password') }}" autocomplete="off" id="update-password">
                    <div class="card-body admin-profile-page-card-body">
                        @csrf
                        @method('put')

                        <div class="form-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                            <label>{{ __('Current Password') }}</label>
                            <input type="password" name="old_password" id="old_password" class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}" placeholder=" " value="">
                            @include('alerts.feedback', ['field' => 'old_password'])
                            <span class="old_password_err" style="color: red;"></span>
                        </div>
                        <!-- <span class="old_password_err" style="color: red;"></span> -->
                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <label>{{ __('New Password') }}</label>
                            <input type="password" name="password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder=" " value="">
                            @include('alerts.feedback', ['field' => 'password'])
                        <span class="password_err" style="color: red;"></span>
                        </div>
                        <!-- <span class="password_err" style="color: red;"></span> -->
                        <div class="form-group">
                            <label>{{ __('Confirm New Password') }}</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder=" " value="">
                        <span class="password_confirmation_err" style="color: red;"></span>
                        </div>
                    </div>
                    <!-- <span class="password_confirmation_err" style="color: red;"></span> -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary">{{ __('Change password') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $('#edit-profile').submit(function(e) {

        var name = $('#name').val();
        if(name.length == 0)
        {
          e.preventDefault();
          $('.name_err').text('Name is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
        }
        var email = $('#email').val();
        if(email.length == 0)
        {
          e.preventDefault();
          $('.email_err').text('Email is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
        }

    });

    $('#update-password').submit(function(e) {

        var old_password = $('#old_password').val();
        if(old_password.length == 0)
        {
          e.preventDefault();
          $('.old_password_err').text('Current password is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
        }
        var password = $('#password').val();
        if(password.length == 0)
        {
          e.preventDefault();
          $('.password_err').text('New password is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
        }
        var password_confirmation = $('#password_confirmation').val();
        if(password_confirmation.length == 0)
        {
          e.preventDefault();
          $('.password_confirmation_err').text('Confirm new password is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
        }

    });
   
</script>
@endsection
