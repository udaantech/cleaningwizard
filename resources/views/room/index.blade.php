@extends('layouts.app')

@section('content')
<div class="row screen-location-page scrennn-update-page">
  <div class="col-md-12">
    <div class="top-whit-bg ">
 
	<div class=" page-header-section">
      <div class="icon-heading"><span><img src="{{ asset('images/room-type1.png') }}" class="add-location"></span><p>{{ __('Room Type') }}</p></div>

      <div class="button-set">  
        <button class="btn btn-add-task"  data-toggle="modal" data-target="#add_room_model">Add New Room</button>
      </div>         
   </div>

	<div class="room_listing tabledata">
	  <table class="table-striped">
	    <thead>
	      <tr>
	        <th>Creation Date</th>
	        <th>Room Type</th> 
	        <th class="action_cleaning_wizard">Action</th>
	      </tr>
	    </thead>
      <tbody>
        @if(count($rooms) > 0)
        @foreach($rooms as $room)
         <tr>
           <td><?php echo ucfirst( date_format(date_create($room->created_at),"d/m/Y")) ?></td>
           <td>{{$room->room_type}}</td>
           <td class="action action_cleaning_wizard_room_type">
            <a class="edit_popup_data margin-right-20" href="JavaScript:Void(0);" data-toggle="modal" data-target="#edit_room_model" data-id="{{$room->id}}" data-roomtype="{{$room->room_type}}"><img src="{{asset('images/edit.png')}}">Edit</a>
            <a class="delete-link" href="#" data-toggle="modal" data-target="#deletemodal" data-id="{{$room->id}}"><img src="{{asset('images/delete.png')}}">Delete</a>
          </td>
         </tr>
        @endforeach
        @else
        <tr>
          <td colspan="8" style="text-align: center;">No Data Found.</td>
        </tr>
        @endif
      </tbody>
	  </table>
    {{ $rooms->links() }}
	</div>
</div>
</div>
</div>

<!-- Add New Cleaner Modal -->
<div class="modal fade" id="add_room_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="{{route('room.store')}}" id="add_room_form">
      @csrf
      <div class="Modal-Header">
        <h3>Add New Room</h3><a class=" modal-cancel-btn" data-dismiss="modal">X</a>
      </div>
      <div class="Modal-body">
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="form-group">
              <label>Room Type</label>
              <input class="form-control" type="text" name="room_type" id="room_type">
              <span class="room_type_err" style="color: red;"></span>
            </div>
          </div>
      </div>
      </div>
      <div class="Modal-footer text-right">
        <button type="button" class="btn modal-cancel-btn btn-export" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn modal-save-btn btn-add-task  margin-left-15" id="submit">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- Edit Cleaner Modal -->
<div class="modal fade" id="edit_room_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="{{route('room.update')}}" id="edit_room_form">
      @csrf
      <input class="form-control" type="hidden" name="room_id" id="room_id">
      <div class="Modal-Header">
        <h3>Edit Room Type</h3><a class=" modal-cancel-btn" data-dismiss="modal">X</a>
      </div>
      <div class="Modal-body">
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="form-group">
              <label>Room Type</label>
              <input class="form-control" type="text" name="room_type" id="room_type1">
              <span class="room_type_err1" style="color: red;"></span>
            </div>
          </div>
      </div>
      </div>
      <div class="Modal-footer text-right">
        <button type="button" class="btn modal-cancel-btn btn-export" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn modal-save-btn btn-add-task margin-left-15" id="submit">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- Delete model -->
<div class="modal fade alert-popup" id="deletemodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Record</h5>
        <button type="button" class="close close-user-modal-delete" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('room.delete')}}" method="post">
        @csrf
      <div class="modal-body">
        <input class="form-control" type="hidden" name="room_id" id="delete-id">  
        Do you want to Delete Room Type?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button  type="submit" class="btn btn-primary btn-primary-user-modal">Yes</button>
      </div>
    </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  var edit_room_type = '';

  $('.room_listing').on('click','.edit_popup_data',function(){
  $('#room_id').val($(this).data('id'));
  $('#room_type1').val($(this).data('roomtype'));
  edit_room_type = $(this).data('roomtype');
  });

  $('.room_listing').on('click','.delete-link',function(){
    $('#delete-id').val($(this).data('id'));
  });

</script>
<script type="text/javascript">
  var rooms = @json($all_room);
    $('#add_room_form').submit(function(e) {

      $.each(rooms, function (key, data) {
      if(data.room_type == $('#room_type').val())
      {
        e.preventDefault();
        $('.room_type_err').text('Room name is already exists.').css('color', 'red').fadeIn("slow").fadeOut(5000);
      }
      
   });

    var room_type = $('#room_type').val();
    if(room_type.length == 0)
    {
      e.preventDefault();
      $('.room_type_err').text('Rom type is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }

    });
       
 </script>
 <script type="text/javascript">
  var rooms = @json($all_room);
    $('#edit_room_form').submit(function(e) {

    $.each(rooms, function (key, data) {
      if($('#room_type1').val() != edit_room_type)
      {
        if(data.room_type == $('#room_type1').val())
        {
          e.preventDefault();
          $('.room_type_err1').text('Room name is already exists.').css('color', 'red').fadeIn("slow").fadeOut(5000);
        }

      }
      
   });

    var room_type = $('#room_type1').val();
    if(room_type.length == 0)
    {
      e.preventDefault();
      $('.room_type_err1').text('Room type is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }

    });
       
 </script>


@endsection
