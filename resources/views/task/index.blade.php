@extends('layouts.app')
@section('content')
<div class="alert alert-success" role="alert" id="success-msg" style="display: none;">
</div>
<div class="alert alert-danger" role="alert" id="error-msg" style="display: none;">
</div>
<div class="row screen-location-page scrennn-update-page">
  <div class="col-md-12">
    <div class="top-whit-bg ">
      <div class=" page-header-section">
        <div class="icon-heading"><span><img src="{{ asset('') }}/images/task1.png" class="add-location"></span> <p>{{ __('Task') }}</p></div>
        <div style="display: none;">
        <form method="POST" action="{{route('exportTasks')}}">
          @csrf
          <input type="hidden" name="filter_name" id="filter_name" value="">
          <input type="hidden" name="filter_city" id="filter_city" value="">
          <input type="hidden" name="filter_zip" id="filter_zip" value="">
          <input type="hidden" name="filter_date" id="filter_date" value="">
            <button type="submit" class="btn btn-export btn-export-export" id="exportExcel1"><img src="{{ asset('images/export.png') }}" > Export CSV</button>
          </form>
          </div>
        <div class="button-set button-set-add-new-reset"> 
          <form method="POST" action="{{route('exportTask')}}">
          @csrf
          <input type="hidden" name="filter_name" id="filter_name" value="">
          <input type="hidden" name="filter_city" id="filter_city" value="">
          <input type="hidden" name="filter_zip" id="filter_zip" value="">
          <input type="hidden" name="filter_date" id="filter_date" value="">
            <button type="submit" class="btn btn-export btn-export-export" id="exportExcel1"><img src="{{ asset('images/export.png') }}" > Export CSV</button>
          </form>
          <!-- <button class="btn btn-export" id="exportExcel1"><img src="{{ asset('images/export.png') }}" > Export Excel</button> -->
          <a href="{{route('task.new_task')}}"><button class="btn btn-add-task">Add New task</button></a>
        </div>         
      </div>


    <div class="search-filter">
      <input type="search" class="form-control" placeholder="Search by Cleaner Name" id="search_name" name="name">
      <select class="form-control" id="search_city" name="city">
        <option value="">Filter By City</option>
        @foreach($cities as $city)
          <option>{{ucfirst($city->city)}}</option>
        @endforeach
      </select>
      <select class="form-control" id="search_zip" name="zip">
        <option value="">Pin Code</option>
         @foreach($pincodes as $pincode)
          <option>{{$pincode->zip}}</option>
        @endforeach
      </select>
      <!-- <select class="form-control ddt"><option>13-10-2020 to 13-10-2020</option></select> -->
      <input class="form-control" type="text" name="daterange" value="Select Date" />
      <button type="button" class="btn apply form-control form-control-apply-delete form-control-apply-task-delete" id="applyFilter"> Apply</button> 
      <a href="javascript:void(0)" id="resetFilter"><button class="form-control-reset form-control-apply-delete">Reset</button></a>
    </div>
  <div class="tabledata task_listing">
    <table>
      <thead>
        <tr>
          <th></th>
          <th>Task Name</th>
          <th>#Task</th>
          <th>Contact Person</th>
          <th>Mobile Number</th>
          <th>Room Type</th>
          <th>Assiged To</th>
        </tr>
      </thead>
      <tbody id="render-filterdata">
    @if(count($tasks) > 0)    
    @foreach($tasks as $key=> $task)
    <tr >
      <td class="accordion-toggle collapsed" id="accordion1" data-toggle="collapse" data-parent="#accordion1" href="#tb{{$key}}"><img src="{{ asset('') }}/images/+-icon.png" class="p-plus"><img src="{{ asset('') }}/images/--icon.png" class="p-min"></td>
        <td>
          <a href="" class="nme view_subtask" data-toggle="modal" data-target="#sub_task_model" data-view_taskname="{{$task['task_name']}}" data-view_subtasks="{{$task['subTasks']}}" data-view_comments="{{$task['comments']}}">{{$task['task_name']}}</a>
          <div class="taskdate">
            Task Created Date
            <span> {{date('d/m/Y', strtotime($task['created_at']))}}</span>
          </div>
        </td>
        <td id="count_{{$task['id']}}">{{count($task['subTasks'])}}</td>
        <td>
          <div class="taskdate margin-bottom-10">
            {{$task['taskLocation']['contact_person']}}
          </div>
           <div class="taskdate">
            Address
            <!-- <span>Moneron  15<sup>th</sup> St 15050 San Francisco ca 94103</span> -->
            <span>{{$task['taskLocation']['address']}} {{$task['location']['area']}}  {{$task['location']['city']}}  {{$task['location']['zip']}}</span>
          </div>
        </td>
        <td>
          {{$task['taskLocation']['contact_number']}}
        </td>
        <td>{{ucfirst($task['room']['room_type'])}}</td>
        <td>{{$task['user']['name']}}</td>
    </tr>
    <tr class="hide-table-padding"> 
      <td colspan="7">
        <div id="tb{{$key}}" class="collapse in p-2">
          <table>
            <tr>
              <th colspan="2">Sub Task</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
            @if(count($task['subTasks']) > 0)    
            @foreach($task['subTasks'] as $k => $subTask)
            
            <tr id="sub_{{$subTask['id']}}">
              <td>{{ucfirst($subTask['task_name'])}}</td>
              <!-- <td>{{$k + 1}}</td> -->
              <td></td>
              <td>
                <select class="form-control" id="changeStatusVal" onChange="changeStatus({{$subTask['id']}})"> 
                  <option value="0" {{ $subTask['status'] == 0 ? 'selected="selected"' : '' }} >Pending</option>
                  <option value="1" {{ $subTask['status'] == 1 ? 'selected="selected"' : '' }} >Issue</option>
                  <option value="2" {{ $subTask['status'] == 2 ? 'selected="selected"' : '' }} >Completed</option>
                </select>
              </td>
              <td class="action"> 
               <!--  <a class="edit_popup_data margin-right-20"  data-toggle="modal" data-target="#sub_taskedit" href="#"><img src="{{asset('images/edit.png')}}">Edit</a> -->
                <a class="edit_popup_data margin-right-20"  data-toggle="modal" data-target="#sub_taskedit" data-sub_task_id="{{$subTask['id']}}" data-sub_task="{{$subTask['task_name']}}" data-status="{{ $subTask['status']}}" href="#"><img src="{{asset('images/edit.png')}}">Edit</a>
                <!-- <a class="delete-link" href="#" id="{{$subTask['id']}}" task-id="{{$task['id']}}"><img src="{{asset('images/delete.png')}}">Delete</a> -->
                <a class="delete-link" href="#" data-toggle="modal" data-target="#deletemodal" data-subtask_id="{{$subTask['id']}}" data-task_id="{{$task['id']}}"><img src="{{asset('images/delete.png')}}">Delete</a>
              </td>
            </tr>
            @endforeach
            @else
            <tr>
              <td colspan="8" style="text-align: center;">No SubTask Found.</td>
            </tr>
            @endif
          </table>
        </div>
      </td>
    </tr>
    @endforeach
    @else
    <tr>
      <td colspan="8" style="text-align: center;">No Data Found.</td>
    </tr>
    @endif
    </tbody>
    </table>
    {{ $tasks->links() }}
</div>


  </div> 
</div>
</div>
  
  <!-- Delete model -->
<div class="modal fade alert-popup" id="deletemodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="Modal-Header Modal-Header-display-delete-record">
         <h5>Delete Record</h5>
        <a class="close close-delete-record" data-dismiss="modal" aria-label="Close">X</a>
      </div>
      <form action="{{route('location.delete')}}" method="post">
        @csrf
       <div class="Modal-body Modal-body-display-delete-record">
        <input type="hidden" name="subtask_id" id="subtask_id">  
        <input type="hidden" name="task_id" id="task_id">  
        Do you want to Delete Sub Task?
      </div>
     <div class="Modal-footer text-right text-right-display-delete-record">
        <button type="button" class="btn btn-export btn-export-display-delete-record" id="cancelDelete" data-dismiss="modal">No</button>
        <button  type="submit" class="btn btn-add-task btn-export-display-delete-record" id="deleteSubTask">Yes</button>
      </div>
    </form>
    </div>
  </div>
</div>

  <div class="modal fade" id="sub_taskedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form method="POST" action="{{route('updateSubTask')}}" id="edit_subtask_form">
        @csrf
         <div class="Modal-Header">
            <h3 id="edit_task_title">Edit SubTask</h3><a class=" modal-cancel-btn" data-dismiss="modal">X</a>
         </div>
        <div class="Modal-body">
         <div class="form-group">
              <label>Sub Task</label>
              <input type="hidden" name="sub_task_id" id="edit_sub_task_id">
              <input class="form-control" type="text" name="sub_task" id="edit_sub_task">
              <span class="edit_sub_task_err" style="color: red;"></span>
            </div>

            <div class="form-group">
              <label>Status</label>
              <select class="form-control" id="edit_status" name="edit_status"> 
                  <option value="0">Pending</option>
                  <option value="1">Issue</option>
                  <option value="2">Completed</option>
                </select>
            </div>
        </div>
        <div class="Modal-footer text-right">
          <button type="button" class="btn modal-cancel-btn btn-export" data-dismiss="modal">Cancel</button>
          <button class="btn modal-save-btn btn-add-task  margin-left-15" id="submit">Save</button>
        </div>
      </form>
     </div>       
   </div>
 </div>
<!-- Sub task Modal -->
  
  <div class="modal fade" id="sub_task_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form method="" action="#">
        @csrf
        <div class="Modal-Header">
          <h3 id="view_subtask_title"></h3><a class=" modal-cancel-btn" data-dismiss="modal">X</a>
        </div>
        <div class="Modal-body">
          <table class="kitc-tbl">
            <tr>
              <th colspan="2">
                Sub Task
              </th>
            </tr>           
            <tbody id="view_subtasks">
             
            </tbody>
            <tfoot>
              <tr>
                <th colspan="2">
                  Comments
                </th>
              </tr>
              <tr>
                <td colspan="2" id="view_subtask_comment">
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- <div class="Modal-footer text-right">
          <button type="button" class="btn modal-cancel-btn btn-export" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn modal-save-btn btn-add-task  margin-left-15" id="submit">Save</button>
        </div> -->
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    function changeStatus(id) {
      var status = $("#changeStatusVal").val();
      $.ajax({
        url:'{{URL("changeStatus")}}',
        type:'POST',
        data:{"id":id,'status':status},
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
        success: function(result) {
          $("#success-msg").html('Status changed successfully!').show().delay(3000).fadeOut()
        },
        error: function(e) {
          $("#error-msg").html('Something went wrong').show().delay(3000).fadeOut()
        }
      })
    }
    $('.task_listing').on('click','.delete-link',function(){
      $('#subtask_id').val($(this).data('subtask_id'));
      $('#task_id').val($(this).data('task_id'));
    });
    $("#deleteSubTask").on('click',function(e){
      e.preventDefault();
      var subId = $('#subtask_id').val();
      var taskId = $('#task_id').val();
      $.ajax({
        url:'{{URL("deleteSubTask")}}/'+subId+"/"+taskId,
        type:'GET',
        success: function(result) {
          $("#cancelDelete").trigger('click')
          $('#count_'+taskId).html(result.count)
          $('#sub_'+subId).remove()
          $("#success-msg").html('Status deleted successfully!').show().delay(3000).fadeOut()
        },
        error: function(e) {
          $("#error-msg").html('Something went wrong').show().delay(3000).fadeOut()
        }
      })
    })
    $(".task_listing").on('click','.edit_popup_data',function(){
      $("#edit_sub_task_id").val($(this).data('sub_task_id'))
      $('#edit_sub_task').val($(this).data('sub_task'));
      $('#edit_status').val($(this).data('status'));
    });

    $("#edit_subtask_form").submit(function(e){
      var subTask = $("#edit_sub_task").val()
      if(subTask == '') {
        e.preventDefault();
        $('.edit_sub_task_err').text('SubTask is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
      }
    })
    /* SUB TASK LISTING*/
    $(".task_listing").on('click','.view_subtask', function(){
      // console.log($(this).data('view_subtasks'))
      var subTasks = $(this).data('view_subtasks')
      var html = '';
      var status = '';
      $.each(subTasks, function(index, element){
        html += '<tr><td>'+element.task_name+'</td>';
        if(element.status == 0)
        {
          status = '<span class="btn btn-warning text-light rounded-pill">Pending</span>';
        }
        else if(element.status == 1)
        {
          status = '<span class="btn btn-danger text-light rounded-pill">Issue</span>';
        }
        else {
          status = '<span class="btn btn-success text-light rounded-pill">Completed</span>'; 
        }
        html += '<td>'+status+'</td></tr>';
      });
      $("#view_subtask_title").html($(this).data('view_taskname'))
      $('#view_subtasks').html(html);
      $('#view_subtask_comment').html($(this).data('view_comments'));
    })

    $("#applyFilter").on('click', function(){
      //alert('sdcs');
      var search_name = $("#search_name").val();
      var search_city = $("#search_city").val();
      var search_zip = $("#search_zip").val();
      var date_range = $('input[name="daterange"]').val();
      filterData(search_name,search_city,search_zip,date_range)
    })
    $("#resetFilter").on('click',function(){ 
      var search_name,search_city,search_zip = '';
      var date_range = 'Select Date';
      $("#search_name").val(search_name);
      $("#search_city").val(search_city);
      $("#search_zip").val(search_zip);
      $('input[name="daterange"]').val(date_range);
      filterData(search_name,search_city,search_zip,date_range)    
    })
    function filterData(search_name, search_city, search_zip,date_range) {
      /*Set Value for export*/
      $("#filter_name").val(search_name)
      $("#filter_city").val(search_city)
      $("#filter_zip").val(search_zip)
      $("#filter_date").val(date_range)
      $.ajax({
         url:"{{ route('task.filter') }}",
         method:'GET',
         data:{search_name:search_name,search_city:search_city,search_zip:search_zip,date_range:date_range,},
         dataType:'html',
         success:function(data)
         {
           $('#render-filterdata').html(data);
         }
      });
    }
  </script>
  <script>
    $(function() {
      $('input[name="daterange"]').daterangepicker({
        opens: 'left',
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
      }, function(start, end, label) {
        //   var date = start.format('MM/DD/YYYY') +"-"+ end.format('MM/DD/YYYY');
        //   console.log(date)
        //   $("#filter_date").val(date)
        // console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
      });
      
      $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
      });
      
      $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
      });
    });

  </script>
@endsection
