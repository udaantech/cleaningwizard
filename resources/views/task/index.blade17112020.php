@extends('layouts.app')
@section('content')
<div class="alert alert-success" role="alert" id="success-msg" style="display: none;">
</div>
<div class="alert alert-danger" role="alert" id="error-msg" style="display: none;">
</div>
<div class="row screen-location-page scrennn-update-page">
  <div class="col-md-12">
    <div class="top-whit-bg ">
      <div class=" page-header-section">
        <div class="icon-heading"><span><img src="{{ asset('') }}/images/task1.png" class="add-location"></span> <p>{{ __('Task') }}</p></div>

        <div class="button-set"> 
          <button class="btn btn-export"><img src="{{ asset('images/export.png') }}"> Export Excel</button>
          <a href="{{route('task.new_task')}}"><button class="btn btn-add-task">Add New task</button></a>
        </div>         
      </div>


      <div class="search-filter">
        <input type="search" class="form-control" placeholder="Search by cleaner Name">
        <select class="form-control"><option>Filter By City</option></select>
        <select class="form-control"><option>Pin Code</option></select>
        <select class="form-control ddt"><option>13-10-2020 to 13-10-2020</option></select>
        <button class="btn apply"> Apply</button> 
    </div>
  <div class="tabledata">
    <table>
      <tr>
        <th></th>
        <th>Task Name</th>
        <th>#Task</th>
        <th>Contact Person</th>
        <th>Mobile Number</th>
        <th>Room Type</th>
        <th>Assiged To</th>
      </tr>
    @foreach($tasks as $key=> $task)
    <tr class="accordion-toggle collapsed" id="accordion1" data-toggle="collapse" data-parent="#accordion1" href="#tb{{$key}}">
      <td class="expand-button"><img src="{{ asset('') }}/images/+-icon.png" class="p-plus"><img src="{{ asset('') }}/images/--icon.png" class="p-min"></td>
        <td>
          <a href="" class="nme" data-toggle="modal" data-target="#sub_task_model_{{$key}}">{{$task['task_name']}}</a>
          <div class="taskdate">
            Task Created Date
            <span> {{date('d/m/Y', strtotime($task['created_at']))}}</span>
          </div>
        </td>
        <td id="count_{{$task['id']}}">{{count($task['subTasks'])}}</td>
        <td>
          <div class="taskdate margin-bottom-10">
            {{$task['contact_person']}}
          </div>
           <div class="taskdate">
            Address
            <!-- <span>Moneron  15<sup>th</sup> St 15050 San Francisco ca 94103</span> -->
            <span>{{$task['address']}}</span>
          </div>
        </td>
        <td>
          {{$task['contact_number']}}
        </td>
        <td>{{ucfirst($task['room']['room_type'])}}</td>
        <td>{{$task['user']['name']}}</td>
    </tr>
    <tr class="hide-table-padding"> 
      <td colspan="7">
        <div id="tb{{$key}}" class="collapse in p-2">
          <table>
            <tr>
              <th colspan="2">Sub Task</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
            @foreach($task['subTasks'] as $k => $subTask)
            
            <tr id="sub_{{$subTask['id']}}">
              <td>{{$subTask['task_name']}}</td>
              <!-- <td>{{$k + 1}}</td> -->
              <td></td>
              <td>
                <select class="form-control" id="changeStatusVal" onChange="changeStatus({{$subTask['id']}})"> 
                  <option value="0" {{ $subTask['status'] == 0 ? 'selected="selected"' : '' }} >Pending</option>
                  <option value="1" {{ $subTask['status'] == 1 ? 'selected="selected"' : '' }} ">Issue</option>
                  <option value="2" {{ $subTask['status'] == 2 ? 'selected="selected"' : '' }} ">Completed</option>
                </select>
              </td>
              <td class="action">
                <a class="edit_popup_data margin-right-20" href="#"><img src="{{asset('images/edit.png')}}">Edit</a>
                <a class="delete-link" href="#" id="{{$subTask['id']}}" task-id="{{$task['id']}}"><img src="{{asset('images/delete.png')}}">Delete</a>
              </td>
            </tr>
            @endforeach
          </table>
        </div>
      </td>
    </tr>
    @endforeach
    </table>
</div>


  </div> 
</div>
</div>

<!-- Sub task Modal -->
  @foreach($tasks as $key=> $task)
  <div class="modal fade" id="sub_task_model_{{$key}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form method="" action="#">
        @csrf
        <div class="Modal-Header">
          <h3>{{$task['task_name']}}</h3><a class=" modal-cancel-btn" data-dismiss="modal">X</a>
        </div>
        <div class="Modal-body">
          <table class="kitc-tbl">
            <tr>
              <th colspan="2">
                Sub Task
              </th>
            </tr>           
            <tbody>
              @foreach($task['subTasks'] as $k => $subTask)
                <tr>
                  <td>
                    {{$subTask['task_name']}}
                  </td>
                  <td>
                    @if($subTask['status'] == 0)
                      <span class="btn btn-warning text-light rounded-pill">Pending</span>
                    @elseif($subTask['status'] == 1)
                      <span class="btn btn-info text-light rounded-pill">Issue</span>
                    @else
                      <span class="btn btn-success text-light rounded-pill">Completed</span>
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th colspan="2">
                  Comments
                </th>
              </tr>
              <tr>
                <td colspan="2">
                   {{$task['comments']}}
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- <div class="Modal-footer text-right">
          <button type="button" class="btn modal-cancel-btn btn-export" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn modal-save-btn btn-add-task  margin-left-15" id="submit">Save</button>
        </div> -->
        </form>
      </div>
    </div>
  </div>
  @endforeach
  <script type="text/javascript">
    function changeStatus(id) {
      var status = $("#changeStatusVal").val();
      $.ajax({
        url:'{{URL("changeStatus")}}',
        type:'POST',
        data:{"id":id,'status':status},
        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
        success: function(result) {
          $("#success-msg").html('Status changed successfully!').show().delay(3000).fadeOut()
        },
        error: function(e) {
          $("#error-msg").html('Something went wrong').show().delay(3000).fadeOut()
        }
      })
    }
    $(".delete-link").on('click',function(e){
      e.preventDefault();
      var subId = $(this).attr('id');
      var taskId = $(this).attr('task-id');
      // console.log(count)
      if (confirm("Do you want to delete?") == true) {
          $.ajax({
            url:'{{URL("deleteSubTask")}}/'+subId+"/"+taskId,
            type:'GET',
            success: function(result) {
              $('#count_'+taskId).html(result.count)
              $('#sub_'+subId).remove()
              $("#success-msg").html('Status deleted successfully!').show().delay(3000).fadeOut()
            },
            error: function(e) {
              $("#error-msg").html('Something went wrong').show().delay(3000).fadeOut()
            }
          })
      } else {
          
      }
    })
  </script>
@endsection
