@extends('layouts.app')

@section('content')
<div class="row screen-location-page scrennn-update-page">
  <div class="col-md-12">
    <form method="POST" action="{{route('task.store')}}" id="add_task_form">
      @csrf
      <div class="top-whit-bg adnew-task">
        <div class=" page-header-section">
          <div class="icon-heading">
              <span><img src="{{ asset('') }}/images/task1.png" class="add-location"></span> <p>{{ __('Add New task') }}</p>
          </div>
          <div class="button-set"> 
            <button type="button" class="btn btn-export cancel">Cancel</button>
            <a href="#"><button class="btn btn-add-task">Save</button></a>
          </div>
        </div>
        <div class="Modal-body margin-top-90">
          <div class="row">
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label>Select City with Area</label>
                <select class="form-control" id="cityArea" name="city_area_id" onchange="addressListing()">
                  <option value="">Select City with Area</option> 
                  @foreach($cityAreas as $cityArea)
                    <option value="{{$cityArea['id']}}">{{$cityArea['city_area']}}</option>
                  @endforeach
                </select>
                <span class="city_err" style="color: red;"></span>
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label>Address</label>
                <select class="form-control" id="address" name="address_id" onchange="getUserId()">
                  <option value="">Select Address</option>
                </select>
                <span class="address_err" style="color: red;"></span>
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label>Select Room Type</label>
                <select class="form-control" id="room_type" name="room_id">
                  <option value="">Room Type</option> 
                  @foreach($roomType as $type)
                    <option value="{{$type->id}}">{{ucfirst($type->room_type)}}</option>
                  @endforeach
                </select>
                <span class="room_err" style="color: red;"></span>
              </div>
              <input type="hidden" name="user_id" id="user_id">
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="form-group">
                <label>Task Name</label>
                <input class="form-control" type="text" name="task_name" id="task_name">
                <span class="task_err" style="color: red;"></span>
              </div>
            </div> 
            <!-- <div class="col-md-12 col-sm-12">
              <div class="form-group">
                <label>Comment</label>
                <textarea class="form-control" type="text" name="comment" id="comment"></textarea>
                <span class="comment_err" style="color: red;"></span>
              </div>
            </div>  -->
          </div>
        </div> 
        <!-- <div class="Modal-body">
          <div class="row">
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label>Address</label>
                <input class="form-control" type="" name="address" id="address">
                <span class="address_err" style="color: red;"></span>
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label>Door Unlock Code</label>
                <input class="form-control" type="text" name="door_unlock_code" id="unlock_code">
                <span class="unlock_code_err" style="color: red;"></span>
              </div>
            </div> 
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label>Contact Person Name</label>
                <input class="form-control" type="text" name="contact_person" id="person_name">
                <span class="person_name_err" style="color: red;"></span>
              </div>
            </div> 
            <div class="col-md-6 col-sm-12">
              <div class="form-group">
                <label>Contact Number</label>
                <input class="form-control" type="number" name="contact_number" id="person_number">
                <span class="person_number_err" style="color: red;"></span>
              </div>
            </div> 
          </div>
        </div>  -->
        <div class="Modal-body">
          <div class="control-group" id="fields">
            <h3>Sub Task Name</h3>        
            <div class="controls"> 
              <form role="form" autocomplete="off">
                <span class="subtask_err" style="color: red;"></span>
              	<div class="formbx">
	                <div class="entry input-group col-xs-3">
	                    <span class="input-group-btn">
	                        <button class="btn btn-success btn-add" type="button">
	                            <span class="fa fa-plus"></span>
	                        </button>
	                    </span><input class="form-control subTask" name="subTasks[]" type="text" placeholder="Type something" />
                  </div>
            	</div>
              </form>
              <br> 
            </div>
          </div>
        </div>
        <div class="Modal-footer">
          <div class="button-set"> 
            <button type="button" class="btn btn-export cancel">Cancel</button>
            <a href="#"><button class="btn btn-add-task">Save</button></a>
          </div>
        </div>
      </div> 
    </form>
  </div>
</div>
<script type="text/javascript">
	$(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
    	//alert();
        e.preventDefault();

        var controlForm = $('.controls form:first'),
        	controlbox = $('.controls .formbx'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlbox);

        newEntry.find('input').val('');
        controlbox.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="fa fa-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
		$(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});

function addressListing() {
  var cityArea = $("#cityArea").val()
  if(cityArea != '') {
    $.ajax({
      type:'GET',
      url:'{{URL("cityAddress")}}/'+cityArea,
      success:function(data) {
        $("#address").empty().append(data)
      }
    }); 
  }
  else
  {
    $("#address").html('<option value="">Select Address</option>');
  }
  getUserId()
}
function getUserId() {
  var cityArea = $("#cityArea").val()
  var address = $("#address").val()
  if(cityArea != '' && address != '') {
    $.ajax({
     type:'GET',
     url:'{{URL("getUserId")}}/'+address,
     success:function(data) {
      $("#user_id").val(data.userId)
     }
    });
  }
  else
  {
    $("#user_id").val('')
  }
}

$(".cancel").on('click',function(){
  window.location.href = '{{route("task.index")}}';
});

</script>
<script type="text/javascript">
  $('#add_task_form').submit(function(e) {
     var cityArea = $('#cityArea').val();

     if(cityArea == '')
     { 
       e.preventDefault();
       $('.city_err').text('City Area is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
     }
     var address = $('#address').val();
     if(address == '')
     {
       e.preventDefault();
       $('.address_err').text('Address is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
     }
     var room_type = $('#room_type').val();
     if(room_type == '')
     {
       e.preventDefault();
       $('.room_err').text('Room Type is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
     }
     var task_name = $('#task_name').val();
     if(task_name == '')
     {
       e.preventDefault();
       $('.task_err').text('Task Name is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
     }
    var subTaskExist = 1;
    $(".subTask").each(function(index,elem){
      
      if($(this).val() != '') {
        subTaskExist = 0;
        return false
      }
    })
    if(subTaskExist == 1) {
      e.preventDefault();
      $('.subtask_err').text('Sub Task is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
    }
  });
       
 </script>
@endsection
