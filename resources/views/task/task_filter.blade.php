@if(count($tasks) > 0)
@foreach($tasks as $key=> $task)
    <tr >
      <td class="accordion-toggle collapsed" id="accordion1" data-toggle="collapse" data-parent="#accordion1" href="#tb{{$key}}"><img src="{{ asset('') }}/images/+-icon.png" class="p-plus"><img src="{{ asset('') }}/images/--icon.png" class="p-min"></td>
        <td>
          <a href="" class="nme view_subtask" data-toggle="modal" data-target="#sub_task_model" data-view_taskname="{{$task['task_name']}}" data-view_subtasks="{{$task['subTasks']}}" data-view_comments="{{$task['comments']}}">{{$task['task_name']}}</a>
          <div class="taskdate">
            Task Created Date
            <span> {{date('d/m/Y', strtotime($task['created_at']))}}</span>
          </div>
        </td>
        <td id="count_{{$task['id']}}">{{count($task['subTasks'])}}</td>
        <td>
          <div class="taskdate margin-bottom-10">
            {{$task['taskLocation']['contact_person']}}
          </div>
           <div class="taskdate">
            Address
            <!-- <span>Moneron  15<sup>th</sup> St 15050 San Francisco ca 94103</span> -->
            <span>{{$task['taskLocation']['address']}} {{$task['location']['area']}}  {{$task['location']['city']}}  {{$task['location']['zip']}}</span>
          </div>
        </td>
        <td>
          {{$task['taskLocation']['contact_number']}}
        </td>
        <td>{{ucfirst($task['room']['room_type'])}}</td>
        <td>{{$task['user']['name']}}</td>
    </tr>
    <tr class="hide-table-padding"> 
      <td colspan="7">
        <div id="tb{{$key}}" class="collapse in p-2">
          <table>
            <tr>
              <th colspan="2">Sub Task</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
            @foreach($task['subTasks'] as $k => $subTask)
            
            <tr id="sub_{{$subTask['id']}}">
              <td>{{$subTask['task_name']}}</td>
              <!-- <td>{{$k + 1}}</td> -->
              <td></td>
              <td>
                <select class="form-control" id="changeStatusVal" onChange="changeStatus({{$subTask['id']}})"> 
                  <option value="0" {{ $subTask['status'] == 0 ? 'selected="selected"' : '' }} >Pending</option>
                  <option value="1" {{ $subTask['status'] == 1 ? 'selected="selected"' : '' }} >Issue</option>
                  <option value="2" {{ $subTask['status'] == 2 ? 'selected="selected"' : '' }} >Completed</option>
                </select>
              </td>
              <td class="action"> 
               <!--  <a class="edit_popup_data margin-right-20"  data-toggle="modal" data-target="#sub_taskedit" href="#"><img src="{{asset('images/edit.png')}}">Edit</a> -->
                <a class="edit_popup_data margin-right-20"  data-toggle="modal" data-target="#sub_taskedit" data-sub_task_id="{{$subTask['id']}}" data-sub_task="{{$subTask['task_name']}}" data-status="{{ $subTask['status']}}" href="#"><img src="{{asset('images/edit.png')}}">Edit</a>
                <a class="delete-link" href="#" id="{{$subTask['id']}}" task-id="{{$task['id']}}"><img src="{{asset('images/delete.png')}}">Delete</a>
              </td>
            </tr>
            @endforeach
          </table>
        </div>
      </td>
    </tr>
    @endforeach
    @else
    <tr>
      <td colspan="8" style="text-align: center;">No Data Found.</td>
    </tr>
    @endif