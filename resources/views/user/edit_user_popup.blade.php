<input class="form-control" type="hidden" name="cleaner_id" value="{{$user->id}}">
<div class="col-md-6 col-sm-6 col-xs-12">
  <div class="form-group">
    <label>First Name</label>
    <input class="form-control" type="text" name="first_name" id="first_name1" value="{{$user->first_name}}" readonly>
    <span class="firstname_err1" style="color: red;"></span>
  </div>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
  <div class="form-group">
    <label>Last Name</label>
    <input class="form-control" type="text" name="last_name" id="last_name1" value="{{$user->last_name}}" readonly>
    <span class="lastname_err1" style="color: red;"></span>
  </div>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
  <div class="form-group">
    <label>Email</label>
    <input class="form-control" type="text" name="email" id="email1" value="{{$user->email}}" readonly>
    <span class="email_err1" style="color: red;"></span>
  </div>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
  <div class="form-group">          
    <label>Phone Number</label>
    <input class="form-control" type="text" name="phone_number" id="phone_number1" value="{{$user->phone}}" readonly>
    <span class="phonenumber_err1" style="color: red;"></span>
  </div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="form-group form-group-clean-gap">
    <!-- <div>Add</div> -->

    <div class="update-cleanwizar-plus"><img  id="edit-add-items" class="add-cleanwizar-plus-img" src="{{asset('images/plus.png')}}" ></div>
  </div>
</div>
@foreach($user->userLocation as $key=> $user_location)
 

  <div class="row cleaningwizard-new-row cleaningwizard-new-row-wizard-gap-clean delete-div{{$key}}" id="update-cleaningwizard-new-row-id">

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="form-group">
        <hr>
      </div>
    </div>
    
    <div class="col-md-12 col-sm-12 col-xs-12 form-make-add-gap-clean">
      <div class="form-group ">
        <div class="update-cleanwizar-minus-delete"><img class="add-cleanwizar-plus-img delete-edit-form-item delete-div{{$key}} " data-id="delete-div{{$key}}" src="{{asset('images/_.png')}}" ></div>
      </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="form-group">
        <label>Assign Location</label>
        <select name="user[{{$key}}][assign_location]" class="cleanwizard_assign_location assign_location_val1">
          <option value="">Select One</option>
          @foreach($locations as $location)
          <option value="{{$location->id}}" {{$location->id === $user_location['location_id'] ? 'selected' : ''}}>{{$location->city}}/{{$location->area}}</option>
          @endforeach
        </select>
      </div>
    </div>
    <!-- <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="form-group">
        <label>Room Type</label>
        <select name="user[{{$key}}][room_type]" class="cleanwizard_assign_location room_type_val1">
          <option value="">Select One</option>
          @foreach($rooms as $room)
          <option value="{{$room->id}}" {{$room->id === $user_location['room_id'] ? 'selected' : ''}}>{{$room->room_type}}</option>
          @endforeach
        </select>
      </div>
    </div> -->
    <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="form-group">          
        <label>Door Unlock Code</label>
        <input class="form-control door_code_val1" type="text" name="user[{{$key}}][door_code]" value="{{$user_location['door_unlock_code']}}">
      </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="form-group">          
        <label>Contact Person Name</label>
        <input class="form-control contact_person_val1" type="text" name="user[{{$key}}][contact_person]" value="{{$user_location['contact_person']}}">
      </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="form-group">          
        <label>Contact Number</label>
        <input class="form-control contact_number_val1" type="text" name="user[{{$key}}][contact_number]" value="{{$user_location['contact_number']}}">
      </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="form-group">          
        <label>Address</label>
        <input class="form-control address_val1" type="text" name="user[{{$key}}][address]" value="{{$user_location['address']}}" placeholder="Apartment, suite, unit, building, street no, floor etc">
      </div>
    </div>

  </div>
<script type="text/javascript">
  var edit_address = @json($edit_addresses);
</script>
@endforeach
<!--  -->




