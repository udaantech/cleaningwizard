@extends('layouts.app')

@section('content')
<div class="row screen-location-page scrennn-update-page">
  <div class="col-md-12">
    <div class="top-whit-bg ">

     <div class=" page-header-section">
      <div class="icon-heading"><span><img src="{{ asset('images/user1.png') }}" class="add-location"></span><p>{{ __('Users') }}</p></div>

      <div class="button-set">  
        <button class="btn btn-add-task"  data-toggle="modal" data-target="#add_cleaner_model">Add New Cleaner</button>
      </div>         
    </div>

    <div class="user_listing tabledata">
     <table class="table-striped">
       <thead>
         <tr>
           <th>Cleaner Name</th>
           <th>Email ID</th>
           <th>Phone Number</th>
           <th>Assigned City with Area</th>
           <th>Action</th>
         </tr>
       </thead>
       <tbody>
         @if(count($users) > 0)
         @foreach($users as $user)
         <tr>
           <td>{{$user->name}}</td>
           <td>{{$user->email}}</td>
           <td>{{$user->phone}}</td>
           <td>
             @foreach($user->userLocation as $u_location)
             {{$u_location->location['city']}}/{{$u_location->location['area']}}<br>
             @endforeach  
           </td>
           <td class="action">
             <a class="edit_popup_data margin-right-20" href="JavaScript:Void(0);" data-toggle="modal" data-select="<?php echo implode(',', $user->userLocation->pluck("location_id")->toArray()) ?>," data-target="#edit_cleaner_model" data-id="{{$user->id}}" data-firstname="{{$user->first_name}}" data-lastname="{{$user->last_name}}" data-email="{{$user->email}}" data-phone="{{$user->phone}}" data-location="{{$user->location}}"><img src="{{asset('images/edit.png')}}">Edit</a>
             <a class="delete-link" href="#" data-toggle="modal" data-target="#deletemodal" data-id="{{$user->id}}"><img src="{{asset('images/delete.png')}}">Delete</a>
           </td>
         </tr>
         @endforeach
         @else
        <tr>
          <td colspan="8" style="text-align: center;">No Data Found.</td>
        </tr>
        @endif
       </tbody>
     </table>
     {{ $users->links() }}
   </div>
 </div>
</div>
</div>

<!-- Add New Cleaner Modal -->
<div class="modal fade" id="add_cleaner_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="{{route('user.store')}}" id="add_cleaner_form">
        @csrf
        <div class="Modal-Header">
          <h3>Add New Cleaner</h3><a class=" modal-cancel-btn" data-dismiss="modal">X</a>
        </div>
        <div class="Modal-body modal-body-row-gap-clean">
          <div class="row">

            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label>First Name</label>
                <input class="form-control" type="text" name="first_name" id="first_name">
                <span class="firstname_err" style="color: red;"></span>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label>Last Name</label>
                <input class="form-control" type="text" name="last_name" id="last_name">
                <span class="lastname_err" style="color: red;"></span>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label>Email</label>
                <input class="form-control" type="text" name="email" id="email">
                <span class="email_err" style="color: red;"></span>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">          
                <label>Phone Number</label>
                <input class="form-control" type="text" name="phone_number" id="phone_number">
                <span class="phonenumber_err" style="color: red;"></span>
              </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="form-group ">
                <div class="add-cleanwizar-plus"><img id="add-cleanwizar-plus" class="add-cleanwizar-plus-img" src="{{asset('images/plus.png')}}" ></div> 
              </div>
            </div>
          </div>
        </div>
        <div class="Modal-body ">
          <div class="row cleaningwizard-new-row delete-added-div-first" id="cleaningwizard-new-row-id">  
           

            <div class="row delete-added-div-row-margin delete-added-div-first">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label>Assign Location</label>
                  <select name="user[0][assign_location]" class="cleanwizard_assign_location assign_location_val-data">
                    <option value="">Select One</option>
                    @foreach($locations as $location)
                    <option value="{{$location->id}}">{{$location->city}}/{{$location->area}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label>Room Type</label>
                  <select name="user[0][room_type]" class="cleanwizard_assign_location room_type_val">
                    <option value="">Select One</option>
                    @foreach($rooms as $room)
                    <option value="{{$room->id}}">{{$room->room_type}}</option>
                    @endforeach
                  </select>
                </div>
              </div> -->
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">          
                  <label>Door Unlock Code</label>
                  <input class="form-control door_code_val" type="text" name="user[0][door_code]">
                  
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">          
                  <label>Contact Person Name</label>
                  <input class="form-control contact_person_val" type="text" name="user[0][contact_person]">
                  
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">          
                  <label>Contact Number</label>
                  <input class="form-control contact_number_val" type="text" name="user[0][contact_number]">
                  
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">          
                  <label>Address</label>
                  <input class="form-control address_val" type="text" name="user[0][address]" placeholder="Apartment, suite, unit, building, street no, floor etc">
                  
                </div>
              </div>
              <!--  -->
            </div>

          </div>
          <div class="row" id="add-fields">
          
          </div> 
          <div class="assign_location_err" style="color: red;"></div>
          <!-- <div class="room_type_err" style="color: red;"></div> -->
          <div class="door_code_err" style="color: red;"></div>
          <div class="contactperson_err" style="color: red;"></div>
          <div class="contactno_err" style="color: red;"></div>
          <div class="address_err" style="color: red;"></div>
        </div> 
        <div class="Modal-footer text-right">
          <button type="button" class="btn modal-cancel-btn btn-export" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn modal-save-btn btn-add-task  margin-left-15" id="submit">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Edit Cleaner Modal -->
<div class="modal fade" id="edit_cleaner_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="{{route('user.update')}}" id="edit_cleaner_form">
        @csrf
        <input class="form-control" type="hidden" name="cleaner_id" id="cleaner_id">
        <div class="Modal-Header">
          <h3>Edit Cleaner</h3><a class=" modal-cancel-btn" data-dismiss="modal">X</a>
        </div>
        <div class="Modal-body">
          <div class="row render-data">

          </div>
          <div class="row" id="update-fields">

          </div>
          <div class="assign_location_err1" style="color: red;"></div>
          <!-- <div class="room_type_err1" style="color: red;"></div> -->
          <div class="door_code_err1" style="color: red;"></div>
          <div class="contactperson_err1" style="color: red;"></div>
          <div class="contactno_err1" style="color: red;"></div>
          <div class="address_err1" style="color: red;"></div> 
        </div>
        <div class="Modal-footer text-right">
          <button type="button" class="btn modal-cancel-btn btn-export" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn modal-save-btn btn-add-task margin-left-15" id="submit">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Delete model -->
<div class="modal fade alert-popup" id="deletemodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Record</h5>
        <button type="button" class="close close-user-modal-delete" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('user.delete')}}" method="post">
        @csrf
        <div class="modal-body">
          <input class="form-control" type="hidden" name="cleaner_id" id="delete-id">  
          Do you want to Delete Cleaner?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
          <button  type="submit" class="btn btn-primary btn-primary-user-modal ">Yes</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
var edit_address_accept = [];
$('.user_listing').on('click','.edit_popup_data',function(){
  var cleaner_id = $(this).data('id');
  $.ajax({
   url:"{{ route('user.edit_popup') }}",
   method:'GET',
   data:{cleaner_id:cleaner_id},
   dataType:'html',
   success:function(data)
   {
    $('.render-data').html(data);
    var numItems = $('.assign_location_val1').length;
    if(numItems == 1)
    {
      $('.render-data .form-make-add-gap-clean').hide();
    }
    edit_address_accept.push(edit_address);
    //console.log(edit_address);
    
  }
});
  
  /*$('#first_name1').val($(this).data('firstname'));
  $('#last_name1').val($(this).data('lastname'));
  $('#email1').val($(this).data('email'));
  $('#phone_number1').val($(this).data('phone'));
  $('#assign_location1').val($(this).data('location'));
  var select = $(this).data('select').split(",");
  $('.editselect').selectpicker('refresh');
  $('.editselect').selectpicker('val', select);*/
});

$('.user_listing').on('click','.delete-link',function(){
  $('#delete-id').val($(this).data('id'));
});

</script>
<script type="text/javascript">
$('#add_cleaner_form').submit(function(e) {

  var address_arr = [];

  var first_name = $('#first_name').val();
  if(first_name.length == 0)
  {
    e.preventDefault();
    $('.firstname_err').text('First name is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
  }
  var last_name = $('#last_name').val();
  if(last_name.length == 0)
  {
    e.preventDefault();
    $('.lastname_err').text('Last name is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
  }
  var phone_number = $('#phone_number').val();
  if(phone_number.length == 0)
  {
    e.preventDefault();
    $('.phonenumber_err').text('Phone number is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
  }
  var email = $('#email').val();
  if(email.length == 0)
  {
    e.preventDefault();
    $('.email_err').text('Email is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
  }
    
  var assignLocationExist = 0;
  $(".assign_location_val").each(function(index,elem){
    if($(this).val() == '') {
      assignLocationExist = 1;
      return false
    }
  })
  if(assignLocationExist == 1) {
    e.preventDefault();
    $('.assign_location_err').text('* Assign location is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
  }

  /*var roomtypeExist = 0;
  $(".room_type_val").each(function(index,elem){
    if($(this).val() == '') {
      roomtypeExist = 1;
      return false
    }
  })
  if(roomtypeExist == 1) {
    e.preventDefault();
    $('.room_type_err').text('* Room type is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
  }*/

  var doorCodeExist = 0;
  $(".door_code_val").each(function(index,elem){
    if($(this).val() == '') {
      doorCodeExist = 1;
      return false
    }
  })
  if(doorCodeExist == 1) {
    e.preventDefault();
    $('.door_code_err').text('* Door unlock code is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
  }

  var contachPersonExist = 0;
  $(".contact_person_val").each(function(index,elem){
    if($(this).val() == '') {
      contachPersonExist = 1;
      return false
    }
  })
  if(contachPersonExist == 1) {
    e.preventDefault();
    $('.contactperson_err').text('* Contact person is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
  }

  var contachNumberExist = 0;
  $(".contact_number_val").each(function(index,elem){
    if($(this).val() == '') {
      contachNumberExist = 1;
      return false
    }
  })
  if(contachNumberExist == 1) {
    e.preventDefault();
    $('.contactno_err').text('* Contact number is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
  }
  
  var addresses = @json($addresses);
  var n = addresses.includes("Mango");

  var addressExist = 0;
  var addressValueExist = 0;
  $(".address_val").each(function(index,elem){
    address_arr.push($(this).val());
    if($(this).val() == '') {
      addressExist = 1;
      return false
    }
    if(addresses.includes($(this).val()))
    {
       addressValueExist = 1;
       return false
    }
  })
  if(addressExist == 1) {
    e.preventDefault();
    $('.address_err').text('* Address is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
  }
  if(addressValueExist == 1) {
    e.preventDefault();
    $('.address_err').text('* This address is already exists.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
  }

  var duplicate = 0;
  for(i=0; i<address_arr.length; i++)
  {
    for(j=i+1; j<address_arr.length; j++)
    {
      if(address_arr[i] == address_arr[j])
      {
         duplicate = 1;
         break;
      }
    }
    if(duplicate)
    {
      break;
    }
  }
  if(duplicate)
  {
    e.preventDefault();
    $('.address_err').text('* Address must be diffrent in all address fields.').css('color', 'red').fadeIn("slow").fadeOut(5000);
  }

  

});

</script>
<script type="text/javascript">
$('#edit_cleaner_form').submit(function(e) {
  
  var address_arr = [];

  var first_name = $('#first_name1').val();
  if(first_name.length == 0)
  {
    e.preventDefault();
    $('.firstname_err1').text('First name is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
  }
  var last_name = $('#last_name1').val();
  if(last_name.length == 0)
  {
    e.preventDefault();
    $('.lastname_err1').text('Last name is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
  }
  var phone_number = $('#phone_number1').val();
  if(phone_number.length == 0)
  {
    e.preventDefault();
    $('.phonenumber_err1').text('Phone number is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
  }
  var email = $('#email1').val();
  if(email.length == 0)
  {
    e.preventDefault();
    $('.email_err1').text('Email is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);
  }
  
  var assignLocationExist = 0;
    $(".assign_location_val1").each(function(index,elem){
      if($(this).val() == '') {
        assignLocationExist = 1;
        return false
      }
    })
    if(assignLocationExist == 1) {
      e.preventDefault();
      $('.assign_location_err1').text('* Assign location is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
    }

    /*var roomtypeExist = 0;
    $(".room_type_val1").each(function(index,elem){
      if($(this).val() == '') {
        roomtypeExist = 1;
        return false
      }
    })
    if(roomtypeExist == 1) {
      e.preventDefault();
      $('.room_type_err1').text('* Room type is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
    }*/

    var doorCodeExist = 0;
    $(".door_code_val1").each(function(index,elem){
      if($(this).val() == '') {
        doorCodeExist = 1;
        return false
      }
    })
    if(doorCodeExist == 1) {
      e.preventDefault();
      $('.door_code_err1').text('* Door unlock code is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
    }

    var contachPersonExist = 0;
    $(".contact_person_val1").each(function(index,elem){
      
      if($(this).val() == '') {
        contachPersonExist = 1;
        return false
      }
    })
    if(contachPersonExist == 1) {
      e.preventDefault();
      $('.contactperson_err1').text('* Contact person is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
    }

    var contachNumberExist = 0;
    $(".contact_number_val1").each(function(index,elem){
      
      if($(this).val() == '') {
        contachNumberExist = 1;
        return false
      }
    })
    if(contachNumberExist == 1) {
      e.preventDefault();
      $('.contactno_err1').text('* Contact number is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
    }
    
    var editAddresses = edit_address_accept[0];
    var addressExist = 0;
    var editAddressValueExist = 0;
    $(".address_val1").each(function(index,elem){
      address_arr.push($(this).val());
      if($(this).val() == '') {
        addressExist = 1;
        return false
      }
      if(editAddresses.includes($(this).val())) {
        editAddressValueExist = 1;
        return false
      }
    })
    if(addressExist == 1) {
      e.preventDefault();
      $('.address_err1').text('* Address is required.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
    }

    if(editAddressValueExist == 1) {
      e.preventDefault();
      $('.address_err1').text('* Address is already exists.').css('color', 'red').fadeIn("slow").fadeOut(5000);  
    }

    var duplicate = 0;
    for(i=0; i<address_arr.length; i++)
    {
      for(j=i+1; j<address_arr.length; j++)
      {
        if(address_arr[i] == address_arr[j])
        {
           duplicate = 1;
           break;
        }
      }
      if(duplicate)
      {
        break;
      }
    }
    if(duplicate)
    {
      e.preventDefault();
      $('.address_err1').text('* Address must be diffrent in all address fields.').css('color', 'red').fadeIn("slow").fadeOut(5000);
    }

});

</script>
<script type="text/javascript">
var index = 0;

$(document).ready(function(){
  $("#add-cleanwizar-plus").on ("click",function(){

    index += 1;

    var fields = '<div class="row cleaningwizard-new-row cleaningwizard-new-row-wizard delete-added-div'+index+'" id="cleaningwizard-new-row-id "><div class="col-md-12 col-sm-12 col-xs-12"> <div class="form-group"><hr></div></div><div class="col-md-12 col-sm-12 col-xs-12 form-make-add-gap-clean"><div class="form-group "><div class="update-cleanwizar-minus-delete"><img class="add-cleanwizar-plus-img delete-added-div delete-added-div'+index+'" data-id="delete-added-div'+index+'"  src="{{asset('images/_.png')}}" > </div></div></div><div class="col-md-12 col-sm-12 col-xs-12"><div class="form-group"><label>Assign Location</label><select name="user['+index+'][assign_location]" class="cleanwizard_assign_location assign_location_val"><option value="">Select One</option>@foreach($locations as $location)<option value="{{$location->id}}">{{$location->city}}/{{$location->area}}</option>@endforeach</select></div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group"><label>Door Unlock Code</label><input class="form-control door_code_val" type="text" name="user['+index+'][door_code]"></div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group"><label>Contact Person Name</label><input class="form-control contact_person_val" type="text" name="user['+index+'][contact_person]"></div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group"><label>Contact Number</label><input class="form-control contact_number_val" type="text" name="user['+index+'][contact_number]"></div></div><div class="col-md-12 col-sm-12 col-xs-12"><div class="form-group"><label>Address</label><input class="form-control address_val" type="text" name="user['+index+'][address]" placeholder="Apartment, suite, unit, building, street no, floor etc"></div></div></div>';
    $('#add-fields').append(fields);
  });
});          
</script>

<script type="text/javascript">
var index = 100;

$(document).ready(function(){
  $(".render-data").on("click",'#edit-add-items',function(){
    index += 1;

    var fields = ' <div class="row cleaningwizard-new-row cleaningwizard-new-row-wizard-gap-clean delete-div'+index+'" id="update-cleaningwizard-new-row-id"><div class="col-md-12 col-sm-12 col-xs-12"><div class="form-group"><hr></div></div><div class="col-md-12 col-sm-12 col-xs-12 form-make-add-gap-clean"><div class="form-group "><div class="update-cleanwizar-minus-delete"><img class="add-cleanwizar-plus-img delete-edit-form-item delete-div'+index+'" data-id="delete-div'+index+'" src="{{asset('images/_.png')}}"></div></div></div><div class="col-md-12 col-sm-12 col-xs-12"><div class="form-group"><label>Assign Location</label><select name="user['+index+'][assign_location]" class="cleanwizard_assign_location assign_location_val1"><option value="">Select One</option>@foreach($locations as $location)<option value="{{$location->id}}">{{$location->city}}/{{$location->area}}</option>@endforeach</select></div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group"><label>Door Unlock Code</label><input class="form-control door_code_val1" type="text" name="user['+index+'][door_code]"></div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group"><label>Contact Person Name</label><input class="form-control contact_person_val1" type="text" name="user['+index+'][contact_person]"></div></div><div class="col-md-6 col-sm-6 col-xs-12"><div class="form-group"><label>Contact Number</label><input class="form-control contact_number_val1" type="text" name="user['+index+'][contact_number]"></div></div><div class="col-md-12 col-sm-12 col-xs-12"><div class="form-group"><label>Address</label><input class="form-control address_val1" type="text" name="user['+index+'][address]" placeholder="Apartment, suite, unit, building, street no, floor etc"></div></div></div>';
    $('#update-fields').append(fields);

    var numItems = $('.assign_location_val1').length;   
    if(numItems > 1)
    {
      $('.render-data .form-make-add-gap-clean').show();
      $('#update-fields .form-make-add-gap-clean').show();
    }
  });
});          
</script>
<script type="text/javascript">
var index = 100;

$(document).ready(function(){
  $(".render-data").on("click",'.delete-edit-form-item',function(){

    var class_name = $(this).data('id');
    var numItems = $('.assign_location_val1').length;
    $('.render-data .'+class_name).remove();
    
    if(numItems == 2)
    {
      $('.render-data .form-make-add-gap-clean').hide();
      $('#update-fields .form-make-add-gap-clean').hide();
    }

  });
  $("#update-fields").on("click",'.delete-edit-form-item',function(){

    var class_name = $(this).data('id');
    var numItems = $('.assign_location_val1').length;
    
    $('#update-fields .'+class_name).remove();
    if(numItems == 2)
    {
      $('.render-data .form-make-add-gap-clean').hide();
      $('#update-fields .form-make-add-gap-clean').hide();
    }
  });

  $('#add_cleaner_form').on('click','.delete-added-div',function()
  {
    var class_name = $(this).data('id');
    $('#add_cleaner_form .'+class_name).remove();
  })

});          
</script>



@endsection
