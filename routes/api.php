<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::group(['prefix' => 'auth'], function () {
    Route::post('/userLogin', 'API\ApiController@userLogin');
    Route::post('/fpassword', 'API\ApiController@generateOTP');
    Route::post('/verify_otp', 'API\ApiController@verifyOTP');
    Route::post('/reset_password', 'API\ApiController@resetPassword');
    Route::group(['middleware' => 'auth:api'], function() {
        Route::post('/getLocations', 'API\LocationApiController@getLocations');
		Route::post('/update_pic', 'API\ApiController@update_pic');
		Route::get('/profile_info', 'API\ApiController@profile_info');
        Route::post('/update_profile', 'API\ApiController@update_profile');
        Route::get('/get_citylist', 'API\ApiController@get_citylist');
        Route::get('/get_addresslist', 'API\ApiController@get_addresslist');
        Route::post('/update_password', 'API\ApiController@update_password');
        Route::get('/get_roomlist', 'API\ApiController@get_roomlist');
        Route::get('/get_tasklist', 'API\ApiController@get_tasklist');
		Route::post('/update_task', 'API\ApiController@update_task');

    });
// });