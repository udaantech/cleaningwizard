<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {	
  return redirect('/login');
});

Auth::routes();
Route::get('/getproduct', 'ProductController@getProduct');
Route::group(['middleware' => 'auth'], function () {
	Route::resource('location', 'LocationController', ['except' => ['show']]);
	Route::resource('product', 'ProductController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

	Route::get('users','UserController@index')->name('user.index');
	Route::post('user/store','UserController@store')->name('user.store');
	Route::post('user/update','UserController@update')->name('user.update');
	Route::get('user/pop-up-data','UserController@editPopupData')->name('user.edit_popup');
	Route::post('user/delete','UserController@delete')->name('user.delete');
	Route::get('tasks','TaskController@index')->name('task.index');
	Route::get('tasksFilter','TaskController@tasksFilter')->name('task.filter');
	Route::get('task/addNewTask','TaskController@addNewTask')->name('task.new_task');
	Route::get('cityUser/{id}','TaskController@getCityUser')->name('cityUser');
	Route::get('cityAddress/{id}','TaskController@getCityAddress')->name('cityAddress');
	Route::get('roomType/{address}','TaskController@getroomType')->name('roomType');
	Route::get('getUserId/{addressId}','TaskController@getUserId')->name('getUserId');
	Route::post('task/store','TaskController@store')->name('task.store');
	Route::post('changeStatus','TaskController@changeStatus')->name("task.changeStatus");
	Route::post('updateSubTask','TaskController@updateSubTask')->name('updateSubTask');
	Route::get('deleteSubTask/{id}/{taskId}','TaskController@deleteSubTask')->name('deleteSubTask');
	Route::post('exportTask','TaskController@exportTask')->name('exportTask');
	Route::get('locations','LocationController@index')->name('location.index');
	Route::post('location/store','LocationController@store')->name('location.store');
	Route::post('location/update','LocationController@update')->name('location.update');
	Route::post('location/delete','LocationController@delete')->name('location.delete');
	Route::get('rooms','RoomController@index')->name('room.index');
	Route::post('room/store','RoomController@store')->name('room.store');
	Route::post('room/update','RoomController@update')->name('room.update');
	Route::post('room/delete','RoomController@delete')->name('room.delete');
	Route::post('exportTasks','TaskController@exportTasks')->name('exportTasks');

});




